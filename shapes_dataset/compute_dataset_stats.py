import json
import os
import numpy as np

color_dict = {
    0: 'red', 
    1: 'green',
    2: 'blue',
    3: 'blank',
}

shape_dict = {
    0: 'blank',
    1: 'square',
    2: 'triangle',
    3: 'circle',
}

def read_config_from_json(json_filename):
    with open(json_filename, 'r') as json_file: 
        raw_json_data = json.load(json_file)
        json_data = dict()
        for entry in raw_json_data:
            if entry['image_id'] not in json_data:
                json_data[entry['image_id']]=entry['config']
    
    return json_data

def count_pairs(config_dict):
    counts = np.zeros([4,4])
    for id in config_dict.keys():
        config = config_dict[id]
        for k in range(0, 9):
            shape_id = config[2*k]
            if shape_id == 0:
                color_id = 3
            else:
                color_id = config[2*k+1]
            counts[shape_id, color_id] += 1

    return counts
            
                
if __name__=='__main__':
    train_json_filename = '/home/tanmay/Code/GenVQA/GenVQA/shapes_dataset/train_anno.json'
    test_json_filename = '/home/tanmay/Code/GenVQA/GenVQA/shapes_dataset/test_anno.json'
    
    train_json_config = read_config_from_json(train_json_filename)
    test_json_config = read_config_from_json(test_json_filename)

    
    train_count = count_pairs(train_json_config)
    
    print(train_count)

    test_count = count_pairs(test_json_config)
    
    print(test_count)


    
