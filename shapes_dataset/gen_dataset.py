#! /bin/python2
import collections
import sys
import os
import json
import random
from PIL import Image, ImageDraw, ImageColor
from shape_globals import Globals

SHAPES = Globals.SHAPES
COLORS = Globals.COLORS

# shapes: blank, square, triangle, circle (0-3)
# color: red, green, blue, white (DNA if shapes is blank) (0-3)



Block = collections.namedtuple('Block', ['shape', 'color'])
QA = collections.namedtuple('QA', ['q', 'a'])

eps = 10;

OUTDIR = ''
im_id_cnt = 1
qs_id_cnt = 1
num_to_gen = 15000
train_im_id_max = 9999
first = True
f_handle = ''
def gen_image(block_array):
    image = Image.new('RGBA', (300, 300))
    draw = ImageDraw.Draw(image);
    cnt = 0;
    for blk in block_array:
        x1 = (cnt % 3)*100+1 + eps
        x2 = x1+100 - 2*eps 
        y1 = cnt/3*100 +eps 
        y2 = y1+100 - 2*eps 
        x1 +=random.randint(0, 15)
        x2 -=random.randint(0, 15)
        y1 +=random.randint(0, 15)
        y2 -=random.randint(0, 15)
        if blk.shape > 0:
            c1 = 255-random.randint(0, 50)
            c2 = random.randint(0, 70)
            c3 = random.randint(0, 70)
            if blk.color == 0:
                col = 'rgb(%d, %d, %d)' %(c1, c2, c3)
            elif blk.color == 1:
                col = 'rgb(%d, %d, %d)' %(c2, c1, c3)
            elif blk.color == 2:
                col = 'rgb(%d, %d, %d)' %(c2, c3, c1)
                
            if blk.shape == 1: # square
                draw.rectangle((x1,y1, x2, y2), fill = col, outline = 'black')                
            elif blk.shape == 2:
                draw.polygon((x1,y1, x2, y1, (x1+x2)/2, y2), fill = col, outline = 'black')
            else :
                draw.ellipse((x1,y1, x2, y2), fill = col, outline = 'black')
            #else: # diamond
                #draw.polygon((x1,(y1+y2)/2, (x1+x2)/2, y1, x2, (y1+y2)/2, (x1+x2)/2, y2), fill = COLORS[blk.color], outline = 'black')
        cnt = cnt+1


    del draw
    return image

def gen_questions(block_array):

    shapecoltable= [ [0]*3 for i in range(3)]
    # is there a
    num_shapes = 0
    qas = []
    for blk in block_array:
        if blk.shape > 0:
            #col = COLORS[blk.color]
            #shp = SHAPES[blk.shape]
            shapecoltable[blk.shape-1][blk.color] +=1
            num_shapes += 1

    for r in range(3):
        for c in range(3):
            ques = 'Is there a %s %s?' % (COLORS[c], SHAPES[r+1])
            if random.randint(1,2) == 1: # add with 50% probability
                if shapecoltable[r][c] == 0:
                    ans = 'no'
                if shapecoltable[r][c] >0:
                    ans = 'yes'
                qas.append(QA(ques, ans))
    # how many
    for r in range(3):
        for c in range(3):
            if shapecoltable[r][c] > 1:
                ques = 'How many %s %ss are there?' %(COLORS[c], SHAPES[r+1])
                ans = '%d' %(shapecoltable[r][c])
                qas.append(QA(ques, ans))
    ques ='How many shapes are there?'
    ans = '%d' %(num_shapes)
    qas.append(QA(ques,ans))

    # what color
    for r in range(3):
        if sum(shapecoltable[r]) == 1:
            ques = 'What color is the %s?' %(SHAPES[r+1])
            for c in range(3):
                if shapecoltable[r][c] == 1:
                    ans = '%s' %(COLORS[c])
                    qas.append(QA(ques,ans))
                    break;

    # relative position (above/below)
    cnt = 0;
    for blk in block_array:
        if blk.shape > 0 and cnt/3 < 2:
            if shapecoltable[blk.shape-1][blk.color] == 1:
                # unique shape/color
                if block_array[cnt+3].shape > 0:
                    b_c = COLORS[block_array[cnt+3].color]
                    b_s = SHAPES[block_array[cnt+3].shape]
                    ques = 'Is there a %s %s below a %s %s?' % (b_c, b_s, COLORS[blk.color], SHAPES[blk.shape])
                    ans = 'yes'
                    qas.append(QA(ques, ans))
                    # now perturb the question
                    b_c_p = COLORS[(block_array[cnt+3].color+1)%3]
                    ques = 'Is there a %s %s below a %s %s?' % (b_c_p, b_s, COLORS[blk.color], SHAPES[blk.shape])
                    ans = 'no'                    
                    qas.append(QA(ques, ans))
        cnt = cnt+1
        
    
                
#    for qa in qas:
#        print(qa)
    return qas


def enumerate_all_sample(target_set_size, on_train, eset = None):
    global im_id_cnt
    global qs_id_cnt
    global first

    out_set = set()
    empty_config = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    while len(out_set) < target_set_size :
        config = list(empty_config)
        for i in xrange(0, 18,2):
            sample = random.randint(0, 4) - 1
            if sample <= 0:
                config[i] = 0
            else:
                config[i] = sample # set shape
                rand_col = random.randint(0,2)
                config[i+1] = rand_col # set color
                if on_train == 1 and sample == rand_col +1: # enforce held-out
                    rand_col = (rand_col + random.choice([1,-1]))%3
                    config[i+1] = rand_col

        config_str = ''.join(str(c) for c in config)
        if eset != None and config_str in eset: # skip if it's in the eset (pre-existing set)
            continue
        # now add to set
        out_set.add(config_str)

    # generate the examples        
    for config in out_set:
        block_array = []
        for i in xrange(0, 18, 2):
            block_array += [Block(int(config[i]), int(config[i+1])) ]
                    
        qas = gen_questions(block_array)
        im = gen_image(block_array)
        im.save(OUTDIR + '/' +  str(im_id_cnt) + '.jpg')


        for qa in qas:
            if first:
                f_handle.write('{')
                first = False
            else:
                f_handle.write(',{')
            qstr = "\"question_id\": %d, \"image_id\" : %d, \"question\": \"%s\", \"answer\": \"%s\", \"config\": [ " %(qs_id_cnt, im_id_cnt, qa.q, qa.a)
            f_handle.write(qstr)
            for i in xrange(len(block_array)):
                blk = block_array[i]
                if i == len(block_array) -1:
                    blk_str = "%d, %d ]" %(blk.shape, blk.color)
                else:
                    blk_str = "%d, %d, " %(blk.shape, blk.color)

                f_handle.write(blk_str)

            f_handle.write('}\n')
            qs_id_cnt = qs_id_cnt + 1
                    
        im_id_cnt+=1

    return out_set

def enumerate_all(block_array):
    global im_id_cnt
    if im_id_cnt == num_to_gen:
        return
    # base case
    if len(block_array) == 9:

        global qs_id_cnt
        global first
#        print block_array
        qas = gen_questions(block_array)
        im = gen_image(block_array)
        im.save(OUTDIR + '/' +  str(im_id_cnt) + '.jpg')
        if im_id_cnt % 100 == 0:
            print(im_id_cnt)
        # write the questions to 'f_handle'
        for qa in qas:
            if first:
                f_handle.write('{')
                first = False
            else:
                f_handle.write(',{')
            qstr = "\"question_id\": %d, \"image_id\" : %d, \"question\": \"%s\", \"answer\": \"%s\", \"config\": [ " %(qs_id_cnt, im_id_cnt, qa.q, qa.a)
            f_handle.write(qstr)
            for i in range(len(block_array)):
                blk = block_array[i]
                if i == len(block_array) -1:
                    blk_str = "%d, %d ]" %(blk.shape, blk.color)
                else:
                    blk_str = "%d, %d, " %(blk.shape, blk.color)
                    
                f_handle.write(blk_str)
                
            f_handle.write('}\n')
            qs_id_cnt = qs_id_cnt + 1

        im_id_cnt = im_id_cnt+1
                                                                                                        
    else:
        order = range(4);
        corder = range(3);
        random.shuffle(corder);
        random.shuffle(order);
        if im_id_cnt <= train_im_id_max:
            for s in order:
                if s > 0:                    
                    for c in corder:
                        if (s == 1 and c == 0) or (s == 2 and c == 1) or (s == 3 and c == 2):
                            continue;
                        enumerate_all(block_array + [Block(s,c)])   
                else:
                    enumerate_all(block_array + [Block(0,0)])


        else:
            for s in order:
                if s > 0:
                    for c in corder:
                        enumerate_all(block_array + [Block(s,c)])   
                else:
                    enumerate_all(block_array + [Block(0,0)])
                
            
        
if __name__ == "__main__":
    
    outdir = sys.argv[1]
    if not os.path.exists(outdir):
        os.makedirs(outdir)
    anno_path = sys.argv[2]
    random.seed(42)
    OUTDIR = outdir

    f = open(sys.argv[2], 'w')
    f_handle = f
    f.write('[')

    #enumerate_all([])
    print "Generating training examples..."
    config_set = enumerate_all_sample(10000, 1, eset = None)
    print "Generating test examples..."
    # pass the training configs in when generating test examples to avoid duplicates
    enumerate_all_sample(5000, 0, eset = config_set)
    f.write(']')
    f.close()
#    [a b c; d e f; g h i]
    # a = Block(3, 0);
    # b = Block(2, 2);
    # c = Block(2, 0);
    # d = Block(0, 0);
    # e = Block(2, 1);
    # f = Block(0, 0);
    # g = Block(1, 2);
    # h = Block(2, 0);
    # i = Block(2, 2);
    # gen_questions([a, b, c, d, e, f, g, h, i])
    # gen_image([a, b, c, d, e, f, g, h, i])
    
