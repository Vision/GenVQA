#! /bin/python2
import json
import sys

if __name__ == "__main__":

    full_anno_f = sys.argv[1]
    train_anno_f = sys.argv[2]
    test_anno_f = sys.argv[3]
    with open(full_anno_f, 'r') as anno_file:
        data = json.load(anno_file)

    train_data = [q for q in data if q['image_id'] < 10000]
    test_data = [q for q in data if q['image_id'] >= 10000]

    train_f = open(train_anno_f, 'w')
    test_f =  open(test_anno_f, 'w')
    json.dump(train_data, train_f)
    json.dump(test_data, test_f)
