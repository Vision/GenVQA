#! /bin/sh
mkdir -p images
python2 gen_dataset.py images anno.json
python2 gen_regions.py  anno.json regions_anno.json
python2 split_dataset.py anno.json train_anno.json test_anno.json
