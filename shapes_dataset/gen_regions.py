#! /bin/python2
import json
import sys
from shape_globals import Globals

SHAPES = Globals.SHAPES
COLORS = Globals.COLORS



# given string phrase and image configuration, return bounding box in 300x300 image
# box is in [x1 y1 x2 y2]
def get_region(config, phrase):
    
    region_bbox = [-1,-1,-1,-1]

    phrase_toks = phrase.split(' ');
    if len(phrase_toks) == 1:
        # just a shape
        
        for i in range(0, len(config), 2):
            if SHAPES[config[i]] == phrase_toks[0]:
                cnt = i/2
                x1 = (cnt % 3)*100+1
                x2 = x1+99 
                y1 = cnt/3*100+1 
                y2 = y1+99 
                region_bbox = [x1, y1, x2, y2]
                break
    
    elif len(phrase_toks) == 2: # color+shape
        if phrase_toks[1][-1] == 's': # drop the plural
            phrase_toks[1] = phrase_toks[1][:-1]
            
        for i in range(0, len(config), 2):
            if SHAPES[config[i]] == phrase_toks[1] and COLORS[config[i+1]] == phrase_toks[0]:

                cnt = i/2
                x1 = (cnt % 3)*100+1
                x2 = x1+99 
                y1 = cnt/3*100+1 
                y2 = y1+99 
                region_bbox = [x1, y1, x2, y2]
                break

    elif len(phrase_toks) == 6:
        for i in range(0, 12,2): # iterate through first 2 rows
            if SHAPES[config[i+6]] == phrase_toks[1] and COLORS[config[i+7]] == phrase_toks[0] and SHAPES[config[i]] == phrase_toks[5] and COLORS[config[i+1]] == phrase_toks[4]:
                cnt1 = i/2
                x1 = (cnt1 % 3)*100+1
                x2 = x1+99 
                y1 = cnt1/3*100+1 
                y2 = y1+199
                region_bbox = [x1, y1, x2, y2]

    
    return region_bbox



if __name__ == "__main__":
    f = open(sys.argv[1], 'r') # path to 'anno.json'
    f_out = open(sys.argv[2], 'w') # output json file
    annos = json.load(f)

    curr_im_id = 1
    regions_dict = {}
    anno_out = []
    im_regions = {}
    for anno in annos:
        if anno['image_id'] != curr_im_id: # next image, dump regions
            anno_out.append({'image_id' : curr_im_id, 'regions': im_regions})
            curr_im_id = anno['image_id']
            im_regions = {}# reset the set
            
        qs = anno['question'][:-1]; # ignore the '?'
        toks = qs.split(" ")
        np_1 = None
        np_1_bbox = None
        np_2 = None
        np_2_bbox = None
        rel = None
        rel_bbox = None
        region_dict = {} # maps im_id to region coordinates
        if "Is there a " in qs:
            # existence
            if anno['answer'] == "no":
                # existence is false, nothing to match
                continue
            np_1 = ' '.join(toks[3:5])
            if "below" in qs:
                np_2 = ' '.join(toks[7:9])
                rel = ' '.join(toks[3:9])

        elif "What color is the" in qs:
            np_1 = toks[-1]
        else:
            continue
        
        if not np_1 in im_regions.keys():
            np_1_bbox = get_region(anno['config'], np_1)
            im_regions[np_1] = np_1_bbox
        if not np_2 == None and not np_2 in im_regions.keys():
            np_2_bbox = get_region(anno['config'], np_2)
            im_regions[np_2] = np_2_bbox                
        if not rel == None:
            rel_bbox = get_region(anno['config'], rel)
            im_regions[rel] = rel_bbox
            
    anno_out.append({'image_id' : curr_im_id, 'regions': im_regions})
    curr_im_id = anno['image_id']

            
    dump_str = json.dumps(anno_out, sort_keys=True, indent=4, separators = (',',': '))
    f_out.write(dump_str)
        
    f.close()
    f_out.close()
