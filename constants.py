import socket
hostname = socket.gethostname()

if hostname=='vision-gpu-1':
    from constants_vision_gpu_1 import *
elif hostname=='vision-gpu-2':
    from constants_vision_gpu_2 import *
elif hostname=='crunchy':
    from constants_crunchy import *

