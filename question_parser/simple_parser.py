from nltk.parse.stanford import StanfordDependencyParser
path_to_jar = '/home/tanmay/Code/GenVQA/GenVQA/question_parser/' + \
              'stanford-parser-full-2015-12-09/stanford-parser.jar'
path_to_models_jar = '/home/tanmay/Code/GenVQA/GenVQA/question_parser/' + \
                     'stanford-parser-full-2015-12-09/stanford-parser-3.6.0-models.jar'
dependency_parser = StanfordDependencyParser(
    path_to_jar=path_to_jar, 
    path_to_models_jar=path_to_models_jar)

result = dependency_parser.raw_parse('Potted Plant')
dep = result.next()
triples = list(dep.triples())
for triple in triples:
    head = triple[0]
    relation = triple[1]
    dependent = triple[2]
    print '{} : {}, {}'.format(
        relation,
        head,
        dependent)
    
