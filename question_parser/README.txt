Dependencies:
install the latest stanford parser:
$wget http://nlp.stanford.edu/software/stanford-parser-full-2015-12-09.zip
$unzip stanford-parser-full-2015-12-09.zip


Compile:
$sh compile.sh


Run example:
sh parse.sh example.txt


Output:
|first 2 words| other words | nsubj | other nounphrase words| 
