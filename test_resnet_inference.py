import os
import json
import image_io
import numpy as np
from resnet.synset import *
import resnet.inference as resnet_inference
import tftools.var_collect as var_collect

import tensorflow as tf


def print_prob(prob):
    #print prob
    pred = np.argsort(prob)[::-1]

    # Get top1 label
    top1 = synset[pred[0]]
    print "Top1: ", top1
    # Get top5 label
    top5 = [synset[pred[i]] for i in range(5)]
    print "Top5: ", top5
    return top1


if __name__=='__main__':
    im_h, im_w = (80, 80)

    model_dir = '/home/tanmay/Downloads/pretrained_networks/' + \
                'Resnet/tensorflow-resnet-pretrained-20160509'
    ckpt_filename = os.path.join(model_dir, 'ResNet-L50.ckpt')
    
    img = image_io.imread("/home/tanmay/Code/GenVQA/GenVQA/resnet/schooner.jpg")
    img = image_io.imresize(img, output_size=(im_h, im_w))
    img = img.astype(np.float32)[:,:,[2,1,0]]-resnet_inference.IMAGENET_MEAN_BGR
    
    sess = tf.Session()

    images = tf.placeholder(tf.float32, shape=[1, im_h, im_w, 3], name='images')
    logits = resnet_inference.inference(images, False)
    prob_tensor = tf.nn.softmax(logits, name='prob')

    vars_to_restore = []
    for s in xrange(5):
        vars_to_restore += var_collect.collect_scope('scale'+str(s+1))
    vars_to_restore += var_collect.collect_scope('fc')

    saver = tf.train.Saver(vars_to_restore)
    saver.restore(sess, ckpt_filename)

    all_vars = var_collect.collect_all()
    vars_to_init = [var for var in all_vars if var not in vars_to_restore]

    init = tf.initialize_variables(vars_to_init)
    sess.run(init)

    print "graph restored"

    batch = img.reshape((1, im_h, im_w, 3))
    feed_dict = {images: batch}
    prob = sess.run(prob_tensor, feed_dict=feed_dict)
    print_prob(prob[0])
