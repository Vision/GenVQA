class HtmlWriter():
    def __init__(self, filename):
        self.filename = filename
        self.html_file = open(self.filename, 'w')
        self.html_file.write("""<!DOCTYPE html>\n<html>\n<body>\n<table border="1" style="width:100%"> \n""")
    
    def add_element(self, col_dict):
        self.html_file.write('    <tr>\n')
        for key in range(len(col_dict)):
            self.html_file.write("""    <td>{}</td>\n""".format(col_dict[key]))
        self.html_file.write('    </tr>\n')

    def image_tag(self, image_path, height, width):
        return """<img src="{}" alt="IMAGE NOT FOUND!" height={} width={}>""".format(image_path,height,width)
        
    def close_file(self):
        self.html_file.write('</table>\n</body>\n</html>')
        self.html_file.close()
