import os
import hashlib
import glob
import subprocess
import ujson
import pdb
import re
import nltk
import numpy as np
import image_io
def dump_questions_to_txt(
        json_filename, 
        questions_txt_filename, 
        question_ids_txt_filename):

    print 'Reading json file: {}'.format(json_filename)
    with open(json_filename ,'r') as file:
        data = ujson.load(file)
        
    print 'Writing to txt file: {}'.format(questions_txt_filename)
    with open(questions_txt_filename, 'w') as txt_file:
        for question in data['questions']:
            txt_file.write(question['question'] + '\n')

    print 'Writing to txt file: {}'.format(question_ids_txt_filename)
    with open(question_ids_txt_filename, 'w') as txt_file:
        for question in data['questions']:
            txt_file.write(str(question['question_id']) + '\n')
    

def parse_questions(in_txt, out_txt):
    os.chdir('/home/tanmay/Code/GenVQA/GenVQA/question_parser')
    cmd = ["sh", "/home/tanmay/Code/GenVQA/GenVQA/question_parser/parse.sh", in_txt]
    outfile = open(out_txt, 'w')
    subprocess.call(cmd, stdout=outfile)
    os.chdir('/home/tanmay/Code/GenVQA/GenVQA')


def parse_annotations(input_json, output_json):
    print 'Reading json file: {}'.format(input_json)
    with open(input_json, 'r') as file:
        data = ujson.load(file)

    print 'Parsing annotations ...'
    parsed_anno = dict()
    for anno in data['annotations']:
        question_data= dict()
        for key, value in anno.items():
            if key != 'question_id':
                question_data[key] = value
        parsed_anno[anno['question_id']] = question_data

    print 'Writing constructed dict to file: {}'.format(output_json)
    with open(output_json, 'w') as file:
        ujson.dump(parsed_anno, file, indent=4, sort_keys=True)
            

def write_json_with_parsed_questions(
        input_json,
        questions_txt_filename,
        question_ids_txt_filename,
        output_json):

    print 'Reading json file: {}'.format(input_json)
    with open(input_json ,'r') as file:
        data = ujson.load(file)

    print 'Reading questions txt file: {}'.format(
        questions_txt_filename)
    with open(questions_txt_filename, 'r') as txt_file:
        questions = txt_file.read().splitlines()

    print 'Reading question ids txt file: {}'.format(
        question_ids_txt_filename)
    with open(question_ids_txt_filename, 'r') as txt_file:
        question_ids = txt_file.read().splitlines()
     
    print 'Adding parsed questions to parsed annotations'
    for i, question_id in enumerate(question_ids):
        question = re.split(' *\|{1} *', questions[i])
        parsed_question = {
            'bin_0': question[1],
            'bin_1': question[2],
            'bin_2': question[3],
            'bin_3': question[4],
        }
        data[str(question_id)]['parsed_question'] = \
            parsed_question

    print 'Writing to json file: {}'.format(output_json)
    with open(output_json, 'w') as file:
        ujson.dump(data, file, indent=4, sort_keys=True)
    

def add_mcq_options(anno_json, questions_json, out_json):
    print 'Reading json file: {}'.format(anno_json)
    with open(anno_json, 'r') as file:
        anno = ujson.load(file)
        
    print 'Reading json file: {}'.format(questions_json)
    with open(questions_json, 'r') as file:
        questions = ujson.load(file)['questions']
    
    print 'Adding mcq options to annotations'
    for question in questions:
        question_id = question['question_id']
        anno[str(question_id)]['multiple_choices'] = \
            question['multiple_choices']

    print 'Writing json file: {}'.format(out_json)
    with open(out_json, 'w') as file:
        ujson.dump(anno, file, indent=4, sort_keys=True)


def add_noun_adjective_labels(
        anno_json, 
        questions_txt, 
        question_ids_txt,
        out_json):

    print 'Reading json file: {}'.format(anno_json)
    with open(anno_json, 'r') as file:
        anno = ujson.load(file)

    print 'Reading questions txt file: {}'.format(
        questions_txt)
    with open(questions_txt, 'r') as txt_file:
        questions = txt_file.read().splitlines()

    print 'Reading question ids txt file: {}'.format(
        question_ids_txt)
    with open(question_ids_txt, 'r') as txt_file:
        question_ids = txt_file.read().splitlines()

    lemmatizer = nltk.stem.WordNetLemmatizer()
    for question, question_id in zip(questions, question_ids):
        words = nltk.tokenize.word_tokenize(question)
        nouns = []
        adjectives = []
        for word, pos_tag in nltk.pos_tag(words):
            if pos_tag in ['NN', 'NNS', 'NNP', 'NNPS']:
                nouns.append(lemmatizer.lemmatize(word.lower()))
            elif pos_tag in ['JJ', 'JJR', 'JJS']:
                adjectives.append(lemmatizer.lemmatize(word.lower()))
            
        anno[question_id]['question_nouns'] = nouns
        anno[question_id]['question_adjectives'] = adjectives
        anno[question_id]['question'] = question

    print 'Writing json file: {}'.format(out_json)
    with open(out_json, 'w') as file:
        ujson.dump(anno, file, indent=4, sort_keys=True)
        

    
def create_ans_vocab(vocab_size, anno_filename, out_json):
    print 'Reading json file: {}'.format(anno_filename)
    with open(anno_filename, 'r') as file:
        annotations = ujson.load(file)

    print 'Creating full mcq answer vocabulary ...'
    ans_vocab = dict()
    count = 0
    for question_id, annotation in annotations.items():
        for answer in annotation['multiple_choices']:
            answer = answer.lower()
            if answer not in ans_vocab:
                ans_vocab[answer] = 0.0
            else:
                ans_vocab[answer] += 1.0

    def get_key(item):
        return item[1]
        
    ans_vocab_list = sorted(
        ans_vocab.items(), 
        key=get_key, 
        reverse=True)

    ans_vocab = dict()
    k = min(vocab_size, len(ans_vocab_list))
    print 'Selecting {} answers'.format(k)
    for i in xrange(k):
        ans_vocab[ans_vocab_list[i][0]] = i

    print 'Writing json file: {}'.format(out_json)
    with open(out_json, 'w') as file:
        ujson.dump(ans_vocab, file, indent=4, sort_keys=True)


def list_of_train_question_ids(
        json_anno,
        fraction_of_held_out_images,
        held_out_qids_json,
        train_subset_qids_json):

    with open(json_anno,'r') as file:
        anno_data = ujson.load(file)
    
    imageid_to_qid = dict()

    for qid, data in anno_data.items():
        image_id = data['image_id']
        if image_id not in imageid_to_qid:
            imageid_to_qid[image_id] = []
        
        imageid_to_qid[image_id] += [qid]

    num_images = len(imageid_to_qid)
    
    num_held_out_images = int(np.floor(num_images*fraction_of_held_out_images))

    np.random.seed(0)
    indices = np.random.choice(num_images, num_held_out_images).tolist()
    
    sorted_imageid_qid_pairs = sorted(imageid_to_qid.items(), key=lambda x: x[0])
    
    held_out_question_ids = []
    train_subset_question_ids = []
    for id, pair in enumerate(sorted_imageid_qid_pairs):
        if id in indices:
            held_out_question_ids += pair[1]
        else:
            train_subset_question_ids += pair[1]

    with open(held_out_qids_json,'w') as file:
        ujson.dump(held_out_question_ids, file)
            
    with open(train_subset_qids_json, 'w') as file:
        ujson.dump(train_subset_question_ids, file)

def list_of_val_question_ids(
        json_anno,
        val_qids_json):

    with open(json_anno,'r') as file:
        anno_data = ujson.load(file)
    
    qids = anno_data.keys()

    with open(val_qids_json,'w') as file:
        ujson.dump(qids, file)


def counts_of_question_objects_and_attributes(
        json_anno,
        question_nouns_json,
        question_adjectives_json):

    with open(json_anno, 'r') as file:
        anno_data = ujson.load(file)

    nouns = dict()
    adjectives = dict()
    for question_data in anno_data.values():
        for noun in question_data['question_nouns']:
            if noun not in nouns:
                nouns[noun] = 0
            nouns[noun] += 1

        for adjective in question_data['question_adjectives']:
            if adjective not in adjectives:
                adjectives[adjective] = 0
            adjectives[adjective] += 1

    sorted_nouns = sorted(nouns.items(), key=lambda x: x[1], reverse=True)
    sorted_adjectives = sorted(adjectives.items(), key=lambda x: x[1], reverse=True)

    with open(question_nouns_json, 'w') as file:
        ujson.dump(sorted_nouns, file, indent=4)

    with open(question_adjectives_json, 'w') as file:
        ujson.dump(sorted_adjectives, file, indent=4)
        

def generate_md5hash(datadir):
    image_dir = os.path.join(
        datadir,
        'val2014')
    
    files = glob.glob(os.path.join(image_dir,'*.jpg'))
    md5_hashes = set()
    count = 0
    for file in files:
        count+=1
        print count, len(files)
        try:
            im = image_io.imread(file)
        except:
            print 'Can not read {}'.format(file)
            pass

        md5_hashes.add(hashlib.md5(im).hexdigest())
        
        
    hashes = os.path.join(
        datadir,
        'md5_hash_val2015.json')
    
    with open(hashes, 'w') as file:
        ujson.dump(md5_hashes, file)

def check_clash(vqa_hash, genome_hash):
    with open(vqa_hash,'r') as file:
        vqa_hash = ujson.load(file)

    with open(genome_hash,'r') as file:
        genome = ujson.load(file)

    print 'Here we go'
    count = 0
    for im_hash in list(vqa_hash):
        if im_hash in genome:
            count += 1
            print count, len(vqa_hash)


if __name__=='__main__':
    datadir = '/home/ssd/VQA/'
    mode = 'val'
    questions_json_filename = os.path.join(
        datadir,
        'MultipleChoice_mscoco_' + mode + '2014_questions.json')

    questions_txt_filename = os.path.join(
        datadir,
        'MultipleChoice_mscoco_' + mode + '2014_questions_dump.txt')

    question_ids_txt_filename = os.path.join(
        datadir,
        'MultipleChoice_mscoco_' + mode + '2014_question_ids_dump.txt')

    questions_parsed_txt_filename = os.path.join(
        datadir,
        'MultipleChoice_mscoco_' + mode + '2014_questions_parsed.txt')
    
    annotations_json_filename = os.path.join(
        datadir,
        'mscoco_' + mode + '2014_annotations.json')

    annotations_parsed_json_filename = os.path.join(
        datadir,
        'mscoco_' + mode + '2014_annotations_parsed.json')

    annotations_with_parsed_questions_filename = os.path.join(
        datadir,
        'mscoco_' + mode + '2014_annotations_with_parsed_questions.json')

    answer_vocab_filename = os.path.join(
        datadir,
        'answer_vocab.json')

    nouns_json_filename = os.path.join(
        datadir,
        'mscoco_' + mode + '2014_question_nouns.json')

    adjectives_json_filename = os.path.join(
        datadir,
        'mscoco_' + mode + '2014_question_adjectives.json')

    ans_vocab_size = 5000

    # dump_questions_to_txt(
    #     questions_json_filename,
    #     questions_txt_filename,
    #     question_ids_txt_filename)
    
    # parse_questions(
    #     questions_txt_filename,
    #     questions_parsed_txt_filename)

    # parse_annotations(
    #     annotations_json_filename, 
    #     annotations_parsed_json_filename)

    # write_json_with_parsed_questions(
    #     annotations_parsed_json_filename,
    #     questions_parsed_txt_filename,
    #     question_ids_txt_filename,
    #     annotations_with_parsed_questions_filename)
    
    # add_mcq_options(
    #     annotations_with_parsed_questions_filename, 
    #     questions_json_filename, 
    #     annotations_with_parsed_questions_filename)

    # add_noun_adjective_labels (
    #     annotations_with_parsed_questions_filename, 
    #     questions_txt_filename, 
    #     question_ids_txt_filename,
    #     annotations_with_parsed_questions_filename)

    # if mode=='train':
        # create_ans_vocab(
        #     ans_vocab_size, 
        #     annotations_with_parsed_questions_filename, 
        #     answer_vocab_filename)

        # train_held_out_qids_json = os.path.join(
        #     datadir,
        #     'train_held_out_qids.json')

        # train_subset_qids_json = os.path.join(
        #     datadir,
        #     'train_subset_qids.json')

        # list_of_train_question_ids(
        #     annotations_with_parsed_questions_filename,
        #     0.05,
        #     train_held_out_qids_json,
        #     train_subset_qids_json)
        
    # if mode=='val':
        # val_qids_json = os.path.join(
        #     datadir,
        #     'val_qids.json')

        # list_of_val_question_ids(
        #     annotations_with_parsed_questions_filename,
        #     val_qids_json)
        
    # counts_of_question_objects_and_attributes(
    #     annotations_with_parsed_questions_filename,
    #     nouns_json_filename,
    #     adjectives_json_filename)


    # generate_md5hash(datadir)

    check_clash(
        '/home/ssd/VQA/md5_hash_val2015.json',
        '/home/ssd/VisualGenome/md5_hash.json')
