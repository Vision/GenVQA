function crop_edge_boxes(image_filename, outdir, model)

%% set up opts for edgeBoxes (see edgeBoxes.m)
opts = edgeBoxes;
opts.alpha = 0.65;     % step size of sliding window search
opts.beta  = 0.2;     % nms threshold for object proposals
opts.minScore = .01;  % min score of boxes to detect
opts.maxBoxes = 100;  % max number of boxes to detect
opts.edgeMinMag = 0.1;
opts.edgeMergeThr = 0.5;
opts.clusterMinMag = 0.5;

if ~exist(outdir, 'dir')
    mkdir(outdir);
end

im = imread(image_filename);
[h,w,c] = size(im);
if (c==1)
    im_tmp = zeros(h,w,3);
    im_tmp(:,:,1) = im;
    im_tmp(:,:,2) = im;
    im_tmp(:,:,3) = im;
    im = uint8(im_tmp);
    c = size(im,3);
end

assert(c==3);

boxes = edgeBoxes(im,model,opts);
parfor i=1:size(boxes, 1)
    x = boxes(i,1);
    y = boxes(i,2);
    w = boxes(i,3);
    h = boxes(i,4);
    
    cropped_im = imresize(im(y:y+h-1, x:x+w-1,:), [224, 224]);
    out_file = fullfile(outdir, [num2str(i) '.jpg']);
    imwrite(cropped_im, out_file);
end

csv_file = fullfile(outdir, 'edge_boxes.csv');
csvwrite(csv_file, boxes)


