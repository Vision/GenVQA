addpath(genpath('toolbox-master'));

%% load pre-trained edge detection model and set opts (see edgesDemo.m)
model=load('models/forest/modelBsds'); model=model.model;
model.opts.multiscale=0; model.opts.sharpen=2; model.opts.nThreads=4;

data_dir = '/home/ssd/VQA/val2014';
out_dir = '/home/ssd/VQA/val2014_cropped_large';
mkdir(out_dir);
image_filenames = dir(fullfile(data_dir, '*.jpg'));

num_images = size(image_filenames);
disp(num_images);
for i = 1: num_images
    disp(i)
    [~, image_name, ext] = fileparts(image_filenames(i).name);
    image_full_name = fullfile(data_dir, [image_name ext]);
    image_dirname = fullfile(out_dir, image_name);
    mkdir(image_dirname);
    crop_edge_boxes(image_full_name, image_dirname, model);
end