import sys
import json
import os
import time
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np
import tensorflow as tf
import atr_data_io_helper as atr_data_loader
import tf_graph_creation_helper as graph_creator
import plot_helper as plotter

def train(train_params):
    sess = tf.InteractiveSession()

    x, y, keep_prob = graph_creator.placeholder_inputs()
    _ = graph_creator.obj_comp_graph(x, 1.0)
    g = tf.get_default_graph()
    obj_feat = g.get_operation_by_name('obj/conv2/obj_feat')

    # Object model restorer
    vars_to_restore = tf.get_collection(tf.GraphKeys.VARIABLES, scope='obj')
    print('Variables to restore:')
    print([var.name for var in vars_to_restore])

    obj_saver = tf.train.Saver(vars_to_restore)
    obj_saver.restore(sess, train_params['obj_model_name'] + '-' + \
                      str(train_params['obj_global_step']))

    y_pred = graph_creator.atr_comp_graph(x, keep_prob, obj_feat.outputs[0])
    cross_entropy = graph_creator.loss(y, y_pred)
    param_varnames_list = [
        'obj/conv1/W',
        'obj/conv2/W',
        'obj/fc1/W',
        'atr/conv1/W',
        'atr/conv2/W',
        'atr/fc1/W_obj',
        'atr/fc1/W_atr'        
    ]
    params_dict = graph_creator.get_list_of_variables(param_varnames_list)

    obj_conv_params = [
        params_dict['obj/conv1/W'],
        params_dict['obj/conv2/W'],
    ]
    obj_fc1_params = [params_dict['obj/fc1/W']]

    atr_conv_params = [
        params_dict['atr/conv1/W'],
        params_dict['atr/conv2/W'],
    ]
    atr_fc1_params = [
        params_dict['atr/fc1/W_obj'],
        params_dict['atr/fc1/W_atr'],
    ]

    regularizer_obj_filters = graph_creator.regularize_params(obj_conv_params)
    regularizer_obj_fc1 = graph_creator.regularize_params(obj_fc1_params)
    regularizer_atr_filters = graph_creator.regularize_params(atr_conv_params)
    regularizer_atr_fc1 = graph_creator.regularize_params(atr_fc1_params)

    total_loss = cross_entropy + \
                 1e-1 * regularizer_obj_fc1 + \
                 1e-3 * regularizer_obj_filters + \
                 1e-1 * regularizer_atr_fc1 + \
                 1e-3 * regularizer_atr_filters

    accuracy = graph_creator.evaluation(y, y_pred)

    
    # Collect variables to save or optimize
    vars_to_opt = tf.get_collection(tf.GraphKeys.VARIABLES, scope='atr')
    vars_to_save = vars_to_opt + vars_to_restore

    print('Variables to optimize:')
    print([var.name for var in vars_to_opt])
    print('Variables to save:')
    print([var.name for var in vars_to_save])
    

    # Object and Attribute model saver
    obj_atr_saver = tf.train.Saver(vars_to_save)
    
    # Add optimization op
    train_step = tf.train.AdamOptimizer(train_params['adam_lr']) \
                         .minimize(total_loss, var_list=vars_to_opt)

    # Collect variables to initialize
    all_vars = tf.get_collection(tf.GraphKeys.VARIABLES)
    vars_to_init = [var for var in all_vars if var not in vars_to_restore]
    print('Variables to initialize:')
    print([var.name for var in vars_to_init])
    
    
    outdir = train_params['out_dir']
    if not os.path.exists(outdir):
        os.mkdir(outdir)

    # Training Data
    img_width = 75
    img_height = 75
    train_json_filename = train_params['train_json']
    with open(train_json_filename, 'r') as json_file: 
        raw_json_data = json.load(json_file)
        train_json_data = dict()
        for entry in raw_json_data:
            if entry['image_id'] not in train_json_data:
                train_json_data[entry['image_id']]=entry['config']
        

    image_dir = train_params['image_dir']
    if train_params['mean_image']=='':
        print('Computing mean image')
        mean_image = atr_data_loader.mean_image(train_json_data, image_dir, 
                                                1000, 100, 
                                                img_height, img_width)
    else:
        print('Loading mean image')
        mean_image = np.load(train_params['mean_image'])
    np.save(os.path.join(outdir, 'mean_image.npy'), mean_image)

    # Val Data
    print('Loading validation data')
    val_batch = atr_data_loader.atr_mini_batch_loader(train_json_data, 
                                                      image_dir, mean_image, 
                                                      9501, 499, 
                                                      img_height, img_width)

    feed_dict_val={x: val_batch[0], y: val_batch[1], keep_prob: 1.0}
    
  
    # Start Training
    print('Initializing variables')
    sess.run(tf.initialize_variables(vars_to_init))

    batch_size = 10
    max_epoch = 2
    max_iter = 950
    val_acc_array_iter = np.empty([max_iter*max_epoch])
    val_acc_array_epoch = np.zeros([max_epoch])
    train_acc_array_epoch = np.zeros([max_epoch])
    for epoch in range(max_epoch):
        for i in range(max_iter):
            if i%100==0:
                print('Iter: ' + str(i))
                print('Val Acc: ' + str(accuracy.eval(feed_dict_val)))

            train_batch = atr_data_loader \
                .atr_mini_batch_loader(train_json_data, image_dir, mean_image, 
                                       1+i*batch_size, batch_size, 
                                       img_height, img_width)
            feed_dict_train={x: train_batch[0], y: train_batch[1], keep_prob: 0.5}

            _, current_train_batch_acc = sess.run([train_step, accuracy], 
                                                  feed_dict=feed_dict_train)
            train_acc_array_epoch[epoch] =  train_acc_array_epoch[epoch] \
                                            + current_train_batch_acc
        
        train_acc_array_epoch[epoch] = train_acc_array_epoch[epoch] / max_iter 
        val_acc_array_epoch[epoch] = accuracy.eval(feed_dict_val)

        plotter.plot_accuracies(xdata=np.arange(0,epoch+1)+1, 
                                ydata_train=train_acc_array_epoch[0:epoch+1], 
                                ydata_val=val_acc_array_epoch[0:epoch+1], 
                                xlim=[1, max_epoch], ylim=[0, 1.], 
                                savePath=os.path.join(outdir,
                                                      'acc_vs_epoch.pdf'))

        _ = obj_atr_saver.save(sess, os.path.join(outdir,'obj_atr_classifier'), 
                               global_step=epoch)
            
    
    sess.close()
    tf.reset_default_graph()

if __name__=='__main__':
    train()
