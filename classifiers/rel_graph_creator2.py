import numpy as np
import math
import pdb
import tensorflow as tf
import answer_classifier.ans_data_io_helper as ans_io_helper
from tf_graph_creation_helper import weight_variable, bias_variable, \
    q_bin_embed_graph, conv2d, max_pool_2x2, batchnorm, explicit_feat_graph


class rel_graph_creator():
    def __init__(self, 
                 plholder_dict,
                 keep_prob,
                 obj_feat, 
                 atr_feat, 
                 obj_prob, 
                 atr_prob,
                 vocab_size, 
                 batch_size,
                 graph_config,
                 mode='q_obj_atr', 
                 is_train=True):
        
        self.mode = mode
        self.is_train = plholder_dict['is_train']
        self.keep_prob = keep_prob
        image_regions = plholder_dict['image_regions']

        with tf.name_scope('rel') as rel_graph:
            # Word Vectors
            word_vecs = self.create_word_vecs(vocab_size, 
                                              graph_config['word_vec_dim'])

            # Feature Computations
            q_feat = self.add_q_feat_comp_layer(word_vecs, plholder_dict)
            reg_feat = self.add_reg_feat_comp_layer(image_regions)
            explt_feat = self.add_explt_feat_comp_layer(obj_prob, 
                                                        atr_prob, 
                                                        plholder_dict)

            # Feature Projections (with batch norm)
            feat_proj_dim = graph_config['joint_embed_dim']
            proj_feat = dict()

            proj_feat['q'] = self.feat_proj_layer(q_feat, feat_proj_dim,
                                                  'q_feat_proj_layer')
            
            proj_feat['reg'] = self.feat_proj_layer(reg_feat, feat_proj_dim,
                                                    'reg_feat_proj_layer')

            proj_feat['obj'] = self.feat_proj_layer(obj_feat, feat_proj_dim,
                                                    'obj_feat_proj_layer')
            
            proj_feat['atr'] = self.feat_proj_layer(atr_feat, feat_proj_dim,
                                                    'atr_feat_proj_layer') 

            # Feature Combination
            coeffs = self.mixing_coeffs()
            print coeffs
            num_regions = batch_size*ans_io_helper.num_proposals
            comb_feat = tf.zeros(shape=[num_regions, feat_proj_dim], 
                                 dtype=tf.float32)
            for feat_type, feat in proj_feat.items():
                comb_feat = comb_feat + feat * coeffs[feat_type]
                
            bn_comb_feat = batchnorm(comb_feat, None, self.is_train)
            bn_explt_feat = batchnorm(explt_feat, None, self.is_train)

            # Softmax scores
            final_feat = tf.concat(1, [bn_comb_feat, bn_explt_feat])
            self.rel_score = self.softmax_layer(tf.nn.relu(final_feat), 
                                                batch_size, 
                                                ans_io_helper.num_proposals)
                
    def create_word_vecs(self, vocab_size, word_vec_dim):
        word_vecs = weight_variable([vocab_size,
                                     word_vec_dim],
                                    var_name='word_vecs')
        word_vecs = tf.nn.l2_normalize(word_vecs, 1)
        tf.add_to_collection('regularize',word_vecs)
        return word_vecs
        
    def add_q_feat_comp_layer(self, word_vecs, plholder_dict):
        with tf.name_scope('q_feat_comp_layer') as q_feat_comp_layer:
            bin0_embed = q_bin_embed_graph('bin0', word_vecs, plholder_dict)
            bin1_embed = q_bin_embed_graph('bin1', word_vecs, plholder_dict)
            bin2_embed = q_bin_embed_graph('bin2', word_vecs, plholder_dict)
            bin3_embed = q_bin_embed_graph('bin3', word_vecs, plholder_dict)
            q_feat = tf.concat(1, 
                               [bin0_embed, bin1_embed, bin2_embed, bin3_embed],
                               name='q_feat')
        return q_feat

    def add_reg_feat_comp_layer(self, image_regions):
        with tf.name_scope('reg_feat_comp_layer') as reg_feat_comp_layer:
            with tf.name_scope('conv1') as conv1:
                W_conv1 = weight_variable([5,5,3,4])
                b_conv1 = bias_variable([4])
                a_conv1 = tf.add(conv2d(image_regions, W_conv1), 
                                 b_conv1, name='a')
                h_conv1 = tf.nn.relu(a_conv1, name='h')
                h_pool1 = max_pool_2x2(h_conv1)
                h_conv1_drop = tf.nn.dropout(h_pool1, self.keep_prob, 
                                             name='h_pool_drop')

            with tf.name_scope('conv2') as conv2:
                W_conv2 = weight_variable([3,3,4,8])
                b_conv2 = bias_variable([8])
                a_conv2 = tf.add(conv2d(h_pool1, W_conv2), b_conv2, name='a')
                h_conv2 = tf.nn.relu(a_conv2, name='h')
                h_pool2 = max_pool_2x2(h_conv2)
                h_pool2_drop = tf.nn.dropout(h_pool2, self.keep_prob, 
                                             name='h_pool_drop')
                h_pool2_drop_shape = h_pool2_drop.get_shape()
                reg_feat_dim = reduce(lambda f, g: f*g, 
                                      [dim.value for dim in 
                                       h_pool2_drop_shape[1:]])
                reg_feat = tf.reshape(h_pool2_drop, [-1, reg_feat_dim], 
                                      name='reg_feat')

            tf.add_to_collection('regularize', W_conv1)
            tf.add_to_collection('regularize', W_conv2)
        
        return reg_feat

    def add_explt_feat_comp_layer(self, obj_prob, atr_prob, plholder_dict):
        with tf.name_scope('explicit_feat') as expl_feat:
            explt_feat_list = []
            for bin_num in xrange(4):
                bin_name = 'bin'+ str(bin_num)
                explt_feat_list.append(explicit_feat_graph(bin_name, obj_prob, 
                                        'obj', plholder_dict))
                explt_feat_list.append(explicit_feat_graph(bin_name, atr_prob, 
                                        'atr', plholder_dict))

            concat_explt_feat = tf.concat(1, explt_feat_list, 
                                          name = 'concat_explt_feat')
                                
            return concat_explt_feat

    def feat_proj_layer(self, feat, proj_dim, name_scope):
        with tf.name_scope(name_scope) as fc_layer:
            feat_dim = feat.get_shape()[1].value
            W1 = weight_variable([feat_dim, proj_dim])
            b1 = bias_variable([proj_dim])
            proj_feat1 = tf.add(tf.matmul(feat, W1), b1)
            bn_proj_feat1 = batchnorm(proj_feat1, None, self.is_train)
            W2 = weight_variable([proj_dim, proj_dim])
            b2 = bias_variable([proj_dim])
            bn_proj_feat2 = tf.add(tf.matmul(tf.nn.relu(bn_proj_feat1), W2), b2)

        tf.add_to_collection('regularize', W1)
        tf.add_to_collection('regularize', W2)

        return bn_proj_feat2
        
    def mixing_coeffs(self):
        feat_types = ['q', 'obj', 'atr', 'reg']
        coeffs = dict()
        count = 0;
        for feat_type in feat_types:
            if feat_type in self.mode:
                coeffs[feat_type] = 1.0
            else:
                coeffs[feat_type] = 0.0
            count += coeffs[feat_type]
        coeffs = {k: v/count for k, v in coeffs.items()}
        return coeffs
          
    def softmax_layer(self, feats, batch_size, num_proposals):
        feat_dim = feats.get_shape()[1].value
        with tf.name_scope('softmax_layer') as softmax_layer:
            W = weight_variable([feat_dim, 1])
            b = bias_variable([1])

            vec_logits = tf.add(tf.matmul(feats, W), b, 
                                name='vec_logits')

            logits = tf.reshape(vec_logits,
                                [batch_size, num_proposals])
            
            y_pred = tf.nn.softmax(logits, name='softmax')

        tf.add_to_collection('regularize', W)

        return y_pred  
