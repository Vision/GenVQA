
import sys
import json
import os
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np
import tensorflow as tf
import object_classifiers.train_obj_classifier as obj_trainer
import object_classifiers.eval_obj_classifier as obj_evaluator
import attribute_classifiers.train_atr_classifier as atr_trainer
import attribute_classifiers.eval_atr_classifier as atr_evaluator
#import answer_classifier.train_ans_classifier as ans_trainer
import answer_classifier.train_ans_classifier_simple as ans_trainer
#import region_ranker.train_rel_classifier as rel_trainer
import region_ranker.train_rel_classifier_simple as rel_trainer
#import region_ranker.eval_rel_classifier as rel_evaluator
import region_ranker.eval_rel_classifier_simple as rel_evaluator

workflow = {
    'train_obj': False,
    'eval_obj': False,
    'train_atr': False,
    'eval_atr': False,
    'train_rel': False,
    'eval_rel': True,
    'train_ans': False,
}

obj_classifier_train_params = {
    'out_dir': '/home/tanmay/Code/GenVQA/Exp_Results/Obj_Classifier',
    'adam_lr': 0.001,
    'train_json': '/home/tanmay/Code/GenVQA/GenVQA/shapes_dataset/train_anno.json',
    'image_dir': '/home/tanmay/Code/GenVQA/GenVQA/shapes_dataset/images',
#    'mean_image': '/home/tanmay/Code/GenVQA/Exp_Results/Obj_Classifier/mean_image.npy',
    'mean_image': '',
}

obj_classifier_eval_params = {
    'out_dir': '/home/tanmay/Code/GenVQA/Exp_Results/Obj_Classifier',
    'model_name': '/home/tanmay/Code/GenVQA/Exp_Results/Obj_Classifier/obj_classifier',
    'global_step': 1,
    'test_json': '/home/tanmay/Code/GenVQA/GenVQA/shapes_dataset/test_anno.json',
    'image_dir': '/home/tanmay/Code/GenVQA/GenVQA/shapes_dataset/images',
    'html_dir': '/home/tanmay/Code/GenVQA/Exp_Results/Obj_Classifier/html_dir',
}

atr_classifier_train_params = {
    'out_dir': '/home/tanmay/Code/GenVQA/Exp_Results/Atr_Classifier',
    'adam_lr': 0.001,
    'train_json': '/home/tanmay/Code/GenVQA/GenVQA/shapes_dataset/train_anno.json',
    'image_dir': '/home/tanmay/Code/GenVQA/GenVQA/shapes_dataset/images',
    'obj_model_name': obj_classifier_eval_params['model_name'],
    'obj_global_step': 1,
#    'mean_image': '/home/tanmay/Code/GenVQA/Exp_Results/Obj_Classifier/mean_image.npy',
    'mean_image': '',
}

atr_classifier_eval_params = {
    'out_dir': '/home/tanmay/Code/GenVQA/Exp_Results/Atr_Classifier',
    'model_name': '/home/tanmay/Code/GenVQA/Exp_Results/Atr_Classifier/obj_atr_classifier',
    'global_step': 1,
    'test_json': '/home/tanmay/Code/GenVQA/GenVQA/shapes_dataset/test_anno.json',
    'image_dir': '/home/tanmay/Code/GenVQA/GenVQA/shapes_dataset/images',
    'html_dir': '/home/tanmay/Code/GenVQA/Exp_Results/Atr_Classifier/html_dir',
}

rel_classifier_train_params = {
    'train_json': '/home/tanmay/Code/GenVQA/GenVQA/shapes_dataset/train_anno.json',
    'test_json': '/home/tanmay/Code/GenVQA/GenVQA/shapes_dataset/test_anno.json',
    'regions_json': '/home/tanmay/Code/GenVQA/GenVQA/shapes_dataset/regions_anno.json',
    'parsed_q_json': '/home/tanmay/Code/GenVQA/GenVQA/shapes_dataset/parsed_questions.json',
    'image_dir': '/home/tanmay/Code/GenVQA/GenVQA/shapes_dataset/images',
    'image_regions_dir': '/mnt/ramdisk/image_regions',
    'outdir': '/home/tanmay/Code/GenVQA/Exp_Results/Rel_Classifier_Obj_Atr_Explt_At_End',
    'obj_atr_model': '/home/tanmay/Code/GenVQA/Exp_Results/Atr_Classifier/obj_atr_classifier-1',
    'mode': 'q_obj_atr_reg_explt',
    'adam_lr' : 0.001,
    'crop_n_save_regions': False,
    'max_epoch': 5,
    'batch_size': 10,
    'fine_tune': False,
    'start_model': 0, # Used only if fine_tune is True
}

rel_classifier_eval_params = {
    'train_json': '/home/tanmay/Code/GenVQA/GenVQA/shapes_dataset/train_anno.json',
    'test_json': '/home/tanmay/Code/GenVQA/GenVQA/shapes_dataset/test_anno.json',
    'regions_json': '/home/tanmay/Code/GenVQA/GenVQA/shapes_dataset/regions_anno.json',
    'parsed_q_json': '/home/tanmay/Code/GenVQA/GenVQA/shapes_dataset/parsed_questions.json',
    'image_dir': '/home/tanmay/Code/GenVQA/GenVQA/shapes_dataset/images',
    'image_regions_dir': '/mnt/ramdisk/image_regions',
    'outdir': '/home/tanmay/Code/GenVQA/Exp_Results/Rel_Classifier_Obj_Atr_Explt_At_End',
    'model_basedir': '/home/tanmay/Code/GenVQA/Exp_Results/Rel_Classifier_Obj_Atr_Explt_At_End',
    'model_number': 4,
    'mode': 'q_obj_atr_reg_explt',
    'batch_size': 20,
    'test_start_id': 94645,
    'test_set_size': 143495-94645+1,
}

ans_classifier_train_params = {
    'train_json': '/home/tanmay/Code/GenVQA/GenVQA/shapes_dataset/train_anno.json',
    'test_json': '/home/tanmay/Code/GenVQA/GenVQA/shapes_dataset/test_anno.json',
    'regions_json': '/home/tanmay/Code/GenVQA/GenVQA/shapes_dataset/regions_anno.json',
    'parsed_q_json': '/home/tanmay/Code/GenVQA/GenVQA/shapes_dataset/parsed_questions.json',
    'image_dir': '/home/tanmay/Code/GenVQA/GenVQA/shapes_dataset/images',
    'image_regions_dir': '/mnt/ramdisk/image_regions',
    'outdir': '/home/tanmay/Code/GenVQA/Exp_Results/Ans_Classifier_Margin',
    'rel_model': '/home/tanmay/Code/GenVQA/Exp_Results/Rel_Classifier_Obj_Atr_Explt_At_End/rel_classifier_q_obj_atr_reg_explt-4',
    'obj_atr_model': '/home/tanmay/Code/GenVQA/Exp_Results/Atr_Classifier/obj_atr_classifier-1',
    'adam_lr' : 0.001,
    'mode' : 'q_obj_atr_reg',
    'crop_n_save_regions': False,
    'max_epoch': 5,
    'batch_size': 10,
    'fine_tune': False,
    'start_model': 0, # When fine_tune is false used to pre-initialize q_obj_atr with q model etc
}

if __name__=='__main__':
    if workflow['train_obj']:
        obj_trainer.train(obj_classifier_train_params)
 
    if workflow['eval_obj']:
        obj_evaluator.eval(obj_classifier_eval_params)

    if workflow['train_atr']:
        atr_trainer.train(atr_classifier_train_params)

    if workflow['eval_atr']:
        atr_evaluator.eval(atr_classifier_eval_params)

    if workflow['train_rel']:
        rel_trainer.train(rel_classifier_train_params)

    if workflow['eval_rel']:
        rel_evaluator.eval(rel_classifier_eval_params)

    if workflow['train_ans']:
        ans_trainer.train(ans_classifier_train_params)

