import json
import os
import math
import pdb
import numpy as np
from collections import namedtuple
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from scipy import misc
region = namedtuple('region','image score coord')

def parse_region_anno(json_filename):
    with open(json_filename,'r') as json_file:
        raw_data = json.load(json_file)

    region_anno_dict = dict()
    for entry in raw_data:
        region_anno_dict[entry['image_id']] = entry['regions']
        
    return region_anno_dict

def get_region_coords(img_height, img_width):
    region_coords_ = np.array([[   1,     1,   100,   100],
                             [  101,    1,  200,  100],
                             [  201,    1,  300,  100],
                             [    1,  101,  100,  200],
                             [  101,  101,  200,  200],
                             [  201,  101,  300,  200],
                             [    1,  201,  100,  300],
                             [  101,  201,  200,  300],
                             [  201,  201,  300,  300],
                             [    1,    1,  100,  200],
                             [  101,    1,  200,  200],
                             [  201,    1,  300,  200],
                             [    1,  101,  100,  300],
                             [  101,  101,  200,  300],
                             [  201,  101,  300,  300],
                             [    1,    1,  200,  100],
                             [  101,    1,  300,  100],
                             [    1,  101,  200,  200],
                             [  101,  101,  300,  200],
                             [    1,  201,  200,  300],
                             [  101,  201,  300,  300],
                             [  1,  1,  300,  300]])

    region_coords = np.copy(region_coords_)
    region_coords[:,0] = np.ceil(region_coords[:,0]*img_width/300.0)
    region_coords[:,1] = np.ceil(region_coords[:,1]*img_height/300.0)
    region_coords[:,2] = np.round(region_coords[:,2]*img_width/300.0)
    region_coords[:,3] = np.round(region_coords[:,3]*img_height/300.0)
    print(region_coords)
    return region_coords, region_coords_

def rank_regions2(image, question, region_coords, region_coords_, 
                 gt_regions_for_image, crop=True):

    num_regions, _ = region_coords.shape
    regions = dict()
    
    count = 0;
    no_region_flag = True
    for i in xrange(num_regions):
        x1_ = region_coords_[i,0]
        y1_ = region_coords_[i,1]
        x2_ = region_coords_[i,2]
        y2_ = region_coords_[i,3]
    
        x1 = region_coords[i,0]
        y1 = region_coords[i,1]
        x2 = region_coords[i,2]
        y2 = region_coords[i,3]
        
        if crop:
            cropped_image = image[y1-1:y2, x1-1:x2, :]
        else:
            cropped_image = None

        score = 0
        
        if question==None:
            no_regions_flag = False
        elif 'How many' in question:
            no_regions_flag = True
        else:
            for gt_region in gt_regions_for_image:
                
                gt_x1, gt_y1, gt_x2, gt_y2 = gt_regions_for_image[gt_region]
                if gt_x1==x1_ and gt_x2==x2_ and gt_y1==y1_ and \
                   gt_y2==y2_ and gt_region in question:
                    score = 1
                    no_region_flag = False
                    break
        
        regions[count] = region(image=cropped_image, score=score, 
                                coord=region_coords[i,:])
        count = count + 1

    if no_region_flag==True:
        for i in xrange(num_regions):
            regions[i] = region(image=regions[i].image, score=1.0/num_regions, 
                                coord=regions[i].coord)
    

    return regions

def rank_regions(image, question, region_coords, region_coords_, 
                 gt_regions_for_image, crop=True):

    num_regions, _ = region_coords.shape
    regions = dict()
    coord_list = []
    no_regions_flag = False
    if question is not None:
        if 'How many' in question:
            no_regions_flag = True
        elif 'What color' in question:
            split_question = question.split(" ")
            gt_region = split_question[-1] 
            gt_region = gt_region[:-1]
            if gt_region not in gt_regions_for_image:
                no_regions_flag = True
            else:
                coord_list.append(gt_regions_for_image[gt_region])
        elif 'below' in question:
            split_question = question.split(" ")
            gt_region = " ".join(split_question[3:5])
            if gt_region not in gt_regions_for_image:
                no_regions_flag = True
            else:
                coord_list.append(gt_regions_for_image[gt_region])
            gt_region = " ".join(split_question[7:9])
            gt_region = gt_region[:-1]
            if gt_region not in gt_regions_for_image:
                no_regions_flag = True
            else:
                coord_list.append(gt_regions_for_image[gt_region])
            gt_region = " ".join(split_question[3:9])
            gt_region = gt_region[:-1]
            if gt_region not in gt_regions_for_image:
                no_regions_flag = True
            else:
                coord_list.append(gt_regions_for_image[gt_region])
        elif 'Is there' in question:
            split_question = question.split(" ")
            gt_region = " ".join(split_question[3:5])
            gt_region = gt_region[:-1]
            if gt_region not in gt_regions_for_image:
                no_regions_flag = True
            else:
                coord_list.append(gt_regions_for_image[gt_region])
            
    num_gt_regions = len(coord_list)
    for i in xrange(num_regions):
        x1_ = region_coords_[i,0]
        y1_ = region_coords_[i,1]
        x2_ = region_coords_[i,2]
        y2_ = region_coords_[i,3]
    
        x1 = region_coords[i,0]
        y1 = region_coords[i,1]
        x2 = region_coords[i,2]
        y2 = region_coords[i,3]
        
        if crop:
            cropped_image = image[y1-1:y2, x1-1:x2, :]
        else:
            cropped_image = None

        score = 0.0
        if no_regions_flag:
            score = 1.0/num_regions
        else:    
            for coord in coord_list:
                gt_x1, gt_y1, gt_x2, gt_y2 = coord
                if gt_x1==x1_ and gt_x2==x2_ and gt_y1==y1_ and gt_y2==y2_:
                    score = 1.0/num_gt_regions
                    break;
        
        regions[i] = region(image=cropped_image, score=score, 
                            coord=region_coords[i,:])
                        
    return regions

def get_rel_map(image, scores, region_coords):
    num_regions = region_coords.shape[0]
    h, w, c = image.shape
    rel_map = np.zeros(shape=[h, w, num_regions], dtype=np.float32)
    for i in xrange(num_regions):
        x1 = region_coords[i,0]
        y1 = region_coords[i,1]
        x2 = region_coords[i,2]
        y2 = region_coords[i,3]
        rel_map[y1-1:y2, x1-1:x2, i] = scores[i]

    rel_map = rel_map.max(axis=2)
    rel_map = rel_map
    return rel_map


if __name__=='__main__':
    image_dir = '/home/tanmay/Code/GenVQA/GenVQA/shapes_dataset/images/'

    json_filename = os.path.join('/home/tanmay/Code/GenVQA/GenVQA/',
                                 'shapes_dataset/train_anno.json')
    # region_anno_dict = parse_region_anno(json_filename)

    # image_id = 1
    # question = 'Is there a blue triangle?'
    # region_coords = region_anno_dict[image_id]
    # image = mpimg.imread(os.path.join(image_dir, str(image_id) + '.jpg'))
    # regions = rank_regions(image, question, region_coords)
    # print(regions)
#    count = 0
    # for i in xrange(14999):
    #     count = count + len(region_anno_dict[i+1])
    # print(count)
    # print(len(region_anno_dict[1]))

    with open(json_filename,'r') as json_file:
        raw_data = json.load(json_file)
        
        
    for key in raw_data:
        if key['image_id']==9999:
            print(key['question_id'])
