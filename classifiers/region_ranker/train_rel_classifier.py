import sys
import os
import json
import math
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np
import pdb
import tensorflow as tf
import answer_classifier.ans_data_io_helper as ans_io_helper
import region_ranker.perfect_ranker as region_proposer
import tf_graph_creation_helper as graph_creator
import plot_helper as plotter

val_start_id = 89645
val_set_size = 5000
val_set_size_small = 500

def recall(pred_scores, gt_scores, k):
    inc_order = np.argsort(pred_scores, 0)
    dec_order = inc_order[::-1]
    gt_scores_ordered = gt_scores[dec_order]
    rel_reg_recalled = np.sum(gt_scores_ordered[0:k]!=0)
    rel_reg = np.sum(gt_scores!=0)
    return rel_reg_recalled/(rel_reg+0.00001)


def batch_recall(pred_scores, gt_scores, k):
    batch_size = pred_scores.shape[0]
    batch_recall = 0.0
    for i in xrange(batch_size):
        if k==-1:
            k_ = np.sum(gt_scores[i,:]!=0)
        else:
            k_ = k
        batch_recall += recall(pred_scores[i,:], gt_scores[i,:], k_)

    batch_recall = batch_recall/batch_size

    return batch_recall

def evaluate(region_score_pred, qa_anno_dict, region_anno_dict, parsed_q_dict,
             ans_vocab, vocab, image_dir, mean_image, start_index, val_set_size,
             batch_size, plholder_dict, img_height, img_width, batch_creator, 
             verbose=False):
    
    recall_at_k = 0
    max_iter = int(math.floor(val_set_size/batch_size))
    for i in xrange(max_iter):
        if verbose==True:
            print('Iter: ' + str(i+1) + '/' + str(max_iter))
        region_images, ans_labels, parsed_q, \
        region_scores_vec, partition= batch_creator \
            .ans_mini_batch_loader(qa_anno_dict, region_anno_dict, 
                                   ans_vocab, vocab, image_dir, mean_image, 
                                   start_index+i*batch_size, batch_size, 
                                   parsed_q_dict,
                                   img_height, img_width, 3)
        region_scores = batch_creator.reshape_score(region_scores_vec)

        feed_dict = ans_io_helper \
            .RelFeedDictCreator(region_images, 
                                parsed_q,
                                region_scores,
                                1.0, 
                                plholder_dict,
                                vocab).feed_dict

        region_score_pred_eval = region_score_pred.eval(feed_dict)
        print region_score_pred_eval
    
        recall_at_k += batch_recall(region_score_pred_eval, 
                                    region_scores, -1)
        
    recall_at_k /= max_iter

    return recall_at_k


def train(train_params):
    sess = tf.InteractiveSession()
    train_anno_filename = train_params['train_json']
    test_anno_filename = train_params['test_json']
    parsed_q_filename = train_params['parsed_q_json']
    regions_anno_filename = train_params['regions_json']
    image_dir = train_params['image_dir']
    image_regions_dir = train_params['image_regions_dir']
    outdir = train_params['outdir']
    batch_size = train_params['batch_size']
    obj_atr_model = train_params['obj_atr_model']
    mode = train_params['mode']

    if not os.path.exists(outdir):
        os.mkdir(outdir)

    qa_anno_dict = ans_io_helper.parse_qa_anno(train_anno_filename)
    parsed_q_dict = ans_io_helper.read_parsed_questions(parsed_q_filename)
    region_anno_dict = region_proposer.parse_region_anno(regions_anno_filename)
    ans_vocab, inv_ans_vocab = ans_io_helper.create_ans_dict()
    vocab, inv_vocab = ans_io_helper.get_vocab(qa_anno_dict)

    # Save region crops
    if train_params['crop_n_save_regions'] == True:
        qa_anno_dict_test = ans_io_helper.parse_qa_anno(test_anno_filename)
        ans_io_helper.save_regions(image_dir, image_regions_dir,
                                   qa_anno_dict, region_anno_dict,
                                   1, 94644, 75, 75)
        ans_io_helper.save_regions(image_dir, image_regions_dir,
                                   qa_anno_dict_test, region_anno_dict,
                                   94645, 143495-94645+1, 75, 75)

    
    # Create graph
    g = tf.get_default_graph()
    plholder_dict = \
        graph_creator.placeholder_inputs_rel(ans_io_helper.num_proposals,
                                             len(vocab), mode='gt')
    image_regions = plholder_dict['image_regions']
    y = plholder_dict['gt_scores']
    keep_prob = plholder_dict['keep_prob']

    y_pred_obj = graph_creator.obj_comp_graph(image_regions, 1.0)
    obj_feat_op = g.get_operation_by_name('obj/conv2/obj_feat')
    obj_feat = obj_feat_op.outputs[0]
    y_pred_atr = graph_creator.atr_comp_graph(image_regions, 1.0, obj_feat)
    atr_feat_op = g.get_operation_by_name('atr/conv2/atr_feat')
    atr_feat = atr_feat_op.outputs[0]
    y_pred = graph_creator.rel_comp_graph(plholder_dict,
                                          obj_feat, atr_feat,
                                          y_pred_obj, y_pred_atr, mode,
                                          keep_prob, len(vocab), batch_size)

    accuracy = graph_creator.evaluation(y, y_pred)
    
    cross_entropy = graph_creator.loss(y, y_pred)

    # Collect variables
    params_varnames = [
        'rel/word_embed/word_vecs',
        'rel/conv1/W',
        'rel/conv2/W',
        'rel/conv1/b',
        'rel/conv2/b',
        'rel/fc1/W_reg',
        'rel/fc1/W_q',
        'rel/fc1/W_obj',
        'rel/fc1/W_atr',
        'rel/fc1/W_explt',
        'rel/fc1/b',
        'rel/fc2/W',
        'rel/fc2/b',
    ]

    vars_dict = graph_creator.get_list_of_variables(params_varnames)

    # parameters grouped together
    rel_word_params = [
        vars_dict['rel/word_embed/word_vecs'],
    ]
    
    rel_conv_params = [
        vars_dict['rel/conv1/W'],
        vars_dict['rel/conv2/W'],
    ]
 
    rel_fc_params = [
        vars_dict['rel/fc1/W_reg'],
        vars_dict['rel/fc1/W_q'],
        vars_dict['rel/fc1/W_obj'],
        vars_dict['rel/fc1/W_atr'],
        vars_dict['rel/fc1/W_explt'],
        vars_dict['rel/fc2/W'],
    ]

    # Regularization
    regularizer_rel_word_vecs = graph_creator.regularize_params(rel_word_params)
    regularizer_rel_filters = graph_creator.regularize_params(rel_conv_params)
    regularizer_rel_fcs = graph_creator.regularize_params(rel_fc_params)
    
    total_loss = cross_entropy + \
                 1e-4 * regularizer_rel_word_vecs + \
                 1e-3 * regularizer_rel_filters + \
                 1e-4 * regularizer_rel_fcs

    # Restore weights
    obj_vars = tf.get_collection(tf.GraphKeys.VARIABLES, scope='obj')
    atr_vars = tf.get_collection(tf.GraphKeys.VARIABLES, scope='atr')
    rel_vars = tf.get_collection(tf.GraphKeys.VARIABLES, scope='rel')

    vars_to_save = rel_vars + atr_vars + obj_vars
    vars_to_train = rel_vars[:]
    pretrained_vars = atr_vars + obj_vars

    # Model to save and restore weights from
    model_saver = tf.train.Saver(vars_to_save)
    
    if train_params['fine_tune']==True:
        pretrained_model = os.path.join(outdir, 'rel_classifier_' + mode +'-'+ \
                                        str(train_params['start_model']))
        assert (os.path.exists(pretrained_model)), \
            'Pretrained model does not exist'
        model_saver.restore(sess, pretrained_model)
        pretrained_vars = vars_to_save[:]
        start_epoch = train_params['start_model'] + 1
    else:
        assert (os.path.exists(obj_atr_model)), \
            'Obj_Atr model does not exist'
        obj_atr_restorer = tf.train.Saver(pretrained_vars)
        obj_atr_restorer.restore(sess, obj_atr_model)
        start_epoch = 0

    # Attach optimization ops
    train_step = tf.train.AdamOptimizer(train_params['adam_lr']) \
                         .minimize(total_loss, var_list=vars_to_train)

    # Initialize uninitialized vars
    all_vars = tf.get_collection(tf.GraphKeys.VARIABLES)
    vars_to_init = [var for var in all_vars if var not in pretrained_vars]
    sess.run(tf.initialize_variables(vars_to_init))

    print('-----------------')
    print 'Variables to train:'
    print [var.name for var in vars_to_train]
    print('-----------------')
    print 'Pretrained variables:'
    print [var.name for var in pretrained_vars]
    print('-----------------')
    print 'Variables to initialize:'
    print [var.name for var in vars_to_init]
    print('-----------------')
    print 'Variables to save'
    print [var.name for var in vars_to_save]
    print('-----------------')

    # Load mean image
    mean_image = np.load('/home/tanmay/Code/GenVQA/Exp_Results/' + \
                         'Obj_Classifier/mean_image.npy')

    # Start Training
    max_epoch = train_params['max_epoch']
    max_iter = 4400*2
    val_rec_array_epoch = np.zeros([max_epoch])
    train_rec_array_epoch = np.zeros([max_epoch])

    # Batch creators
    train_batch_creator = ans_io_helper.batch_creator(1, max_iter*batch_size)
    val_batch_creator = ans_io_helper.batch_creator(val_start_id, val_start_id 
                                                    + val_set_size - 1)
    val_small_batch_creator = ans_io_helper.batch_creator(val_start_id, 
                                                          val_start_id + 
                                                          val_set_size_small-1)

    # Check accuracy of restored model
    if train_params['fine_tune']==True:
        restored_recall = evaluate(y_pred, qa_anno_dict, region_anno_dict, 
                                   parsed_q_dict, ans_vocab, 
                                   vocab, image_regions_dir, 
                                   mean_image, val_start_id, 
                                   val_set_size, batch_size,
                                   plholder_dict, 75, 75,
                                   val_batch_creator)
        print('Recall of restored model: ' + str(restored_recall))
    
    # Accuracy filename
    train_recall_txtfile = os.path.join(outdir,'train_recall_'+ mode +'.txt')
    val_recall_txtfile = os.path.join(outdir,'val_recall_'+ mode +'.txt')
                 
    for epoch in range(start_epoch, max_epoch):
        train_batch_creator.shuffle_ids()
        for i in range(max_iter):
        
            train_region_images, train_ans_labels, train_parsed_q, \
            train_region_score_vec, train_partition= train_batch_creator \
                .ans_mini_batch_loader(qa_anno_dict, region_anno_dict, 
                                       ans_vocab, vocab, 
                                       image_regions_dir, mean_image, 
                                       1+i*batch_size, batch_size,
                                       parsed_q_dict,
                                       75, 75, 3)

            train_region_score = train_batch_creator \
                .reshape_score(train_region_score_vec)

            feed_dict_train = ans_io_helper \
                .RelFeedDictCreator(train_region_images, 
                                    train_parsed_q,
                                    train_region_score,
                                    0.5, 
                                    plholder_dict,
                                    vocab).feed_dict

            _, current_train_batch_acc, y_pred_eval, loss_eval = \
                    sess.run([train_step, accuracy, y_pred, total_loss], 
                             feed_dict=feed_dict_train)

            assert (not np.any(np.isnan(y_pred_eval))), 'NaN predicted'

            train_rec_array_epoch[epoch] = train_rec_array_epoch[epoch] + \
                                           batch_recall(y_pred_eval, 
                                                        train_region_score, -1)
        
            if (i+1)%500==0:
                val_recall = evaluate(y_pred, qa_anno_dict, region_anno_dict, 
                                      parsed_q_dict, ans_vocab, vocab,
                                      image_regions_dir, mean_image, 
                                      val_start_id, val_set_size_small,
                                      batch_size, plholder_dict, 75, 75,
                                      val_small_batch_creator)
                
                print('Iter: ' + str(i+1) + ' Val Sm Rec: ' + str(val_recall))

        train_rec_array_epoch[epoch] = train_rec_array_epoch[epoch] / max_iter
        val_rec_array_epoch[epoch] = evaluate(y_pred, qa_anno_dict, 
                                              region_anno_dict, parsed_q_dict, 
                                              ans_vocab, vocab, 
                                              image_regions_dir, mean_image, 
                                              val_start_id, val_set_size, 
                                              batch_size, plholder_dict, 
                                              75, 75, val_batch_creator)

        print('Val Rec: ' + str(val_rec_array_epoch[epoch]) + 
              ' Train Rec: ' + str(train_rec_array_epoch[epoch]))
        
        
        plotter.write_accuracy_to_file(start_epoch, epoch, 
                                       train_rec_array_epoch,
                                       train_params['fine_tune'],
                                       train_recall_txtfile)
        plotter.write_accuracy_to_file(start_epoch, epoch, 
                                       val_rec_array_epoch,
                                       train_params['fine_tune'],
                                       val_recall_txtfile)
      
        save_path = model_saver.save(sess, 
                                     os.path.join(outdir, 'rel_classifier_' + \
                                                  mode), 
                                     global_step=epoch)

    sess.close()
    tf.reset_default_graph()


