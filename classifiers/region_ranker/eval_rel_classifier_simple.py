import sys
import os
import json
import math
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np
import pdb
import tensorflow as tf
import answer_classifier.ans_data_io_helper as ans_io_helper
import region_ranker.perfect_ranker as region_proposer
#import region_ranker.train_rel_classifier as rel_trainer
import region_ranker.train_rel_classifier_simple as rel_trainer
import tf_graph_creation_helper as graph_creator
import rel_graph_creator
import plot_helper as plotter

def eval(eval_params):
    sess = tf.InteractiveSession()
    train_anno_filename = eval_params['train_json']
    test_anno_filename = eval_params['test_json']
    parsed_q_filename = eval_params['parsed_q_json']
    regions_anno_filename = eval_params['regions_json']
    whole_image_dir = eval_params['image_dir']
    image_regions_dir = eval_params['image_regions_dir']
    outdir = eval_params['outdir']
    model_basedir = eval_params['model_basedir']
    model_number = eval_params['model_number']
    mode = eval_params['mode']
    batch_size = eval_params['batch_size']
    test_start_id = eval_params['test_start_id']
    test_set_size = eval_params['test_set_size']
    model = os.path.join(model_basedir, 'rel_classifier_' + mode + \
                         '-' + str(model_number))
    if not os.path.exists(outdir):
        os.mkdir(outdir)

    qa_anno_dict_train = ans_io_helper.parse_qa_anno(train_anno_filename)
    qa_anno_dict = ans_io_helper.parse_qa_anno(test_anno_filename)
    parsed_q_dict = ans_io_helper.read_parsed_questions(parsed_q_filename)
    region_anno_dict = region_proposer.parse_region_anno(regions_anno_filename)
    ans_vocab, inv_ans_vocab = ans_io_helper.create_ans_dict()
    vocab, inv_vocab = ans_io_helper.get_vocab(qa_anno_dict_train)

    
    # Create graph
    g = tf.get_default_graph()
    plholder_dict = \
        graph_creator.placeholder_inputs_rel(ans_io_helper.num_proposals,
                                             len(vocab), mode='gt')
    image_regions = plholder_dict['image_regions']
    y = plholder_dict['gt_scores']
    keep_prob = plholder_dict['keep_prob']

    y_pred_obj = graph_creator.obj_comp_graph(image_regions, 1.0)
    obj_feat_op = g.get_operation_by_name('obj/conv2/obj_feat')
    obj_feat = obj_feat_op.outputs[0]
    y_pred_atr = graph_creator.atr_comp_graph(image_regions, 1.0, obj_feat)
    atr_feat_op = g.get_operation_by_name('atr/conv2/atr_feat')
    atr_feat = atr_feat_op.outputs[0]
    rel_graph = rel_graph_creator.rel_graph_creator(plholder_dict,
                                                    keep_prob,
                                                    obj_feat,
                                                    atr_feat,
                                                    y_pred_obj,
                                                    y_pred_atr,
                                                    len(vocab),
                                                    batch_size,
                                                    graph_creator.graph_config,
                                                    mode,
                                                    False)
    y_pred = rel_graph.rel_score

    # Restore model
    restorer = tf.train.Saver()
    if os.path.exists(model):
        restorer.restore(sess, model)
    else:
        print 'Failed to read model from file ' + model

    # Load mean image
    mean_image = np.load('/home/tanmay/Code/GenVQA/Exp_Results/' + \
                         'Obj_Classifier/mean_image.npy')

    # Batch creator
    test_batch_creator = ans_io_helper.batch_creator(test_start_id,
                                                     test_start_id 
                                                     + test_set_size - 1)

    # Test Recall
    # test_recall = rel_trainer.evaluate(y_pred, qa_anno_dict, 
    #                                    region_anno_dict, parsed_q_dict,
    #                                    ans_vocab, vocab, 
    #                                    image_regions_dir, mean_image, 
    #                                    test_start_id, test_set_size, 
    #                                    batch_size, plholder_dict,
    #                                    75, 75, test_batch_creator,verbose=True)
    
    html_dir = os.path.join(outdir,'rel_html')
    test_recall = rel_trainer.evaluate_with_vis(y_pred, 
                                                qa_anno_dict, 
                                                region_anno_dict, 
                                                parsed_q_dict,
                                                ans_vocab, 
                                                vocab, 
                                                image_regions_dir, 
                                                mean_image, 
                                                test_start_id, 
                                                test_set_size,
                                                batch_size, 
                                                plholder_dict, 
                                                75, 
                                                75, 
                                                test_batch_creator, 
                                                html_dir,
                                                whole_image_dir,
                                                verbose=True)
    print('Test Rec: ' + str(test_recall))
