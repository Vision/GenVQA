class baseclass():
    def __init__(self, a):
        print a

    def baseMethod(self):
        print 'Yeah inheritance'

class derivedclass(baseclass):
    def __init__(self, a, b):
        baseclass.__init__(self, a)
        print b
        self.baseMethod()

a = derivedclass(1,2)
