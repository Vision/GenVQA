import sys
import os
import json
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np
import math
import random
import pdb
import tensorflow as tf
import object_classifiers.obj_data_io_helper as obj_data_loader
import attribute_classifiers.atr_data_io_helper as atr_data_loader
import tf_graph_creation_helper as graph_creator
import ans_graph_creator
import rel_graph_creator
import plot_helper as plotter
import ans_data_io_helper as ans_io_helper
import region_ranker.perfect_ranker as region_proposer 
import time

val_start_id = 89645
val_set_size = 5000
val_set_size_small = 500


def evaluate(accuracy, qa_anno_dict, region_anno_dict, ans_vocab, vocab,
             image_dir, mean_image, start_index, val_set_size, batch_size,
             plholder_dict, img_height, img_width, batch_creator, 
             parsed_q_dict):
    
    correct = 0
    max_iter = int(math.floor(val_set_size/batch_size))
    for i in xrange(max_iter):
        region_images, ans_labels, parsed_q, \
        region_score, partition= batch_creator \
            .ans_mini_batch_loader(qa_anno_dict, region_anno_dict, 
                                   ans_vocab, vocab, image_dir, mean_image, 
                                   start_index+i*batch_size, batch_size, 
                                   parsed_q_dict, 
                                   img_height, img_width, 3)
            
        feed_dict = ans_io_helper.\
                    AnsFeedDictCreator(region_images, ans_labels, parsed_q, 
                                       region_score, 1.0, plholder_dict, 
                                       vocab, False).feed_dict

        correct = correct + accuracy.eval(feed_dict)

    return correct/max_iter


def train(train_params):
    sess = tf.InteractiveSession()
    
    train_anno_filename = train_params['train_json']
    test_anno_filename = train_params['test_json']
    parsed_q_filename = train_params['parsed_q_json']
    regions_anno_filename = train_params['regions_json']
    image_dir = train_params['image_dir']
    image_regions_dir = train_params['image_regions_dir']
    outdir = train_params['outdir']
    rel_model = train_params['rel_model']
    obj_atr_model = train_params['obj_atr_model']
    batch_size = train_params['batch_size']

    if not os.path.exists(outdir):
        os.mkdir(outdir)

    qa_anno_dict = ans_io_helper.parse_qa_anno(train_anno_filename)
    parsed_q_dict = ans_io_helper.read_parsed_questions(parsed_q_filename)
    region_anno_dict = region_proposer.parse_region_anno(regions_anno_filename)
    ans_vocab, inv_ans_vocab = ans_io_helper.create_ans_dict()
    vocab, inv_vocab = ans_io_helper.get_vocab(qa_anno_dict)
#    vocab = ans_io_helper.join_vocab(vocab, ans_vocab)

    # Save region crops
    if train_params['crop_n_save_regions'] == True:
        qa_anno_dict_test = ans_io_helper.parse_qa_anno(test_anno_filename)
        ans_io_helper.save_regions(image_dir, image_regions_dir,
                                   qa_anno_dict, region_anno_dict,
                                   1, 94644, 75, 75)
        ans_io_helper.save_regions(image_dir, image_regions_dir,
                                   qa_anno_dict_test, region_anno_dict,
                                   94645, 143495-94645+1, 75, 75) 

    # Create graph
    g = tf.get_default_graph()
    plholder_dict = graph_creator.placeholder_inputs_ans(len(vocab), 
                                                         len(ans_vocab), 
                                                         mode='gt')

    image_regions = plholder_dict['image_regions']
    questions = plholder_dict['questions']
    keep_prob = plholder_dict['keep_prob']
    y = plholder_dict['gt_answer']
    region_score = plholder_dict['region_score']

    y_pred_obj = graph_creator.obj_comp_graph(image_regions, 1.0)
    obj_feat_op = g.get_operation_by_name('obj/conv2/obj_feat')
    obj_feat = obj_feat_op.outputs[0]
    y_pred_atr = graph_creator.atr_comp_graph(image_regions, 1.0, obj_feat)
    atr_feat_op = g.get_operation_by_name('atr/conv2/atr_feat')
    atr_feat = atr_feat_op.outputs[0]

    rel_graph = rel_graph_creator.rel_graph_creator(plholder_dict,
                                                    1.0,
                                                    obj_feat,
                                                    atr_feat,
                                                    y_pred_obj,
                                                    y_pred_atr,
                                                    len(vocab),
                                                    batch_size,
                                                    graph_creator.graph_config,
                                                    'q_obj_atr_reg_explt',
                                                    False)

    pred_rel_score = rel_graph.rel_score

    # Restore rel, obj and attribute classifier parameters
    rel_vars = tf.get_collection(tf.GraphKeys.VARIABLES, scope='rel')
    obj_vars = tf.get_collection(tf.GraphKeys.VARIABLES, scope='obj')
    atr_vars = tf.get_collection(tf.GraphKeys.VARIABLES, scope='atr')

    obj_atr_saver = tf.train.Saver(obj_vars+atr_vars)
    rel_saver = tf.train.Saver(rel_vars)

    rel_saver.restore(sess, rel_model)
    obj_atr_saver.restore(sess, obj_atr_model)

    ans_graph = ans_graph_creator.ans_graph_creator(plholder_dict,
                                                    obj_feat,
                                                    atr_feat,
                                                    y_pred_obj,
                                                    y_pred_atr,
                                                    vocab,
                                                    inv_vocab,
                                                    ans_vocab,
                                                    batch_size,
                                                    graph_creator.graph_config,
                                                    train_params['mode'],
                                                    True)

    y_pred = ans_graph.cosine_dist

    pred_rel_score_vec = tf.reshape(pred_rel_score, 
                                    [1, batch_size*ans_io_helper.num_proposals])

    y_avg = graph_creator.aggregate_y_pred(y_pred, 
                                           region_score, batch_size, 
                                           ans_io_helper.num_proposals, 
                                           len(ans_vocab))
    
#    cross_entropy = graph_creator.loss(y, y_avg)
    total_loss = graph_creator.margin_loss(y, y_avg, 0.1)
    accuracy = graph_creator.evaluation(y, y_avg)
    
    # Collect variables
    ans_vars = tf.get_collection(tf.GraphKeys.VARIABLES, scope='ans')
    vars_to_regularize = tf.get_collection('regularize')
     
    for var in vars_to_regularize:
        print var.name
        total_loss += 1e-4 * tf.nn.l2_loss(var)

    # Model to restore some of the weights from
    if train_params['mode']=='q':
        partial_model = ''

    elif train_params['mode']=='q_obj_atr' or \
         train_params['mode']=='q_reg':
        partial_model = os.path.join(outdir, 'ans_classifier_q-' + \
                                     str(train_params['start_model']))

    elif train_params['mode']=='q_obj_atr_reg':
        # partial_model = os.path.join(outdir, 'ans_classifier_q_obj_atr-' + \
        #                              str(train_params['start_model']))
        partial_model = ''

    # Fine tune begining with a previous model
    if train_params['fine_tune']==True:
        partial_model = os.path.join(outdir, 'ans_classifier_' + \
                                     train_params['mode'] + '-' + \
                                     str(train_params['start_model']))
        start_epoch = train_params['start_model']+1
        
        partial_restorer = tf.train.Saver()
    else:
        start_epoch = 0
        if train_params['mode']!='q':
            partial_restorer = tf.train.Saver()

    # Restore partial model
    if os.path.exists(partial_model):
        partial_restorer.restore(sess, partial_model)

    # Save trained vars
    model_saver = tf.train.Saver()
    all_vars_without_optim = tf.all_variables()

    # Attach optimization ops
    word_vecs = tf.get_collection('variables','ans/word_vecs')
    # vars_to_train = [var for var in ans_vars if 
    #                  'ans/word_vecs' not in var.name]
    vars_to_train = ans_vars
    train_step = tf.train.AdamOptimizer(train_params['adam_lr']) \
                         .minimize(total_loss, var_list = vars_to_train)
    
    # Initialize vars_to_init
    all_vars = tf.all_variables()
    optimizer_vars = [var for var in all_vars if var not in 
                      all_vars_without_optim]
    
    print('Optimizer Variables: ')
    print([var.name for var in optimizer_vars])
    print('------------------')

    if train_params['mode']=='q':
        vars_to_init = ans_vars + optimizer_vars
    else:
        vars_to_init = ans_vars + optimizer_vars

    sess.run(tf.initialize_variables(vars_to_init))
    
    # Load mean image
    mean_image = np.load('/home/tanmay/Code/GenVQA/Exp_Results/' + \
                         'Obj_Classifier/mean_image.npy')

    # Start Training
    max_epoch = train_params['max_epoch']
    max_iter = 4400*2
    val_acc_array_epoch = np.zeros([max_epoch])
    train_acc_array_epoch = np.zeros([max_epoch])

    # Batch creators
    train_batch_creator = ans_io_helper.batch_creator(1, max_iter*batch_size)
    val_batch_creator = ans_io_helper.batch_creator(val_start_id, val_start_id 
                                                    + val_set_size - 1)
    val_small_batch_creator = ans_io_helper.batch_creator(val_start_id, 
                                                          val_start_id + 
                                                          val_set_size_small-1)

    # Check accuracy of restored model
    # if train_params['fine_tune']==True:
        # restored_accuracy = evaluate(accuracy, qa_anno_dict, 
        #                              region_anno_dict, ans_vocab, 
        #                              vocab, image_regions_dir, 
        #                              mean_image, val_start_id, 
        #                              val_set_size, batch_size,
        #                              plholder_dict, 75, 75,
        #                              val_batch_creator,
        #                              parsed_q_dict)
        # print('Accuracy of restored model: ' + str(restored_accuracy))
    
    # Accuracy filename
    train_accuracy_txtfile = os.path.join(outdir,'train_accuracy_' + \
                                          train_params['mode'] + '.txt')
    val_accuracy_txtfile = os.path.join(outdir,'val_accuracy_' + \
                                        train_params['mode'] + '.txt')

    for epoch in range(start_epoch, max_epoch):
        train_batch_creator.shuffle_ids()
        for i in range(max_iter):
            train_region_images, train_ans_labels, train_parsed_q, \
            train_region_score, train_partition= train_batch_creator \
                .ans_mini_batch_loader(qa_anno_dict, region_anno_dict, 
                                       ans_vocab, vocab, 
                                       image_regions_dir, mean_image, 
                                       1+i*batch_size, batch_size,
                                       parsed_q_dict,
                                       75, 75, 3)

            feed_dict_train = ans_io_helper \
                .AnsFeedDictCreator(train_region_images, 
                                    train_ans_labels, 
                                    train_parsed_q,
                                    train_region_score,
                                    0.5, 
                                    plholder_dict,
                                    vocab,
                                    True).feed_dict            

            _, current_train_batch_acc, y_avg_eval, loss_eval = \
                    sess.run([train_step, accuracy, y_avg, total_loss], 
                             feed_dict=feed_dict_train)
                
            # print(y_avg_eval[0,:])
            # print(train_ans_labels[0,:])

#            rel_logits = g.get_operation_by_name('rel/fc2/vec_logits')
#            print(rel_logits.outputs[0].eval(feed_dict_train))
#            print (pred_rel_score.eval(feed_dict_train))

            assert (not np.any(np.isnan(y_avg_eval))), 'NaN predicted'

            train_acc_array_epoch[epoch] = train_acc_array_epoch[epoch] + \
                                           current_train_batch_acc
        
            if (i+1)%500==0:
                val_accuracy = evaluate(accuracy, qa_anno_dict, 
                                        region_anno_dict, ans_vocab, vocab,
                                        image_regions_dir, mean_image, 
                                        val_start_id, val_set_size_small,
                                        batch_size, plholder_dict, 75, 75,
                                        val_small_batch_creator,
                                        parsed_q_dict)
                
                print('Iter: ' + str(i+1) + ' Val Sm Acc: ' + str(val_accuracy))

        train_acc_array_epoch[epoch] = train_acc_array_epoch[epoch] / max_iter
        val_acc_array_epoch[epoch] = evaluate(accuracy, qa_anno_dict, 
                                              region_anno_dict, ans_vocab, 
                                              vocab, image_regions_dir, 
                                              mean_image, val_start_id, 
                                              val_set_size, batch_size,
                                              plholder_dict, 75, 75,
                                              val_batch_creator,
                                              parsed_q_dict)

        print('Val Acc: ' + str(val_acc_array_epoch[epoch]) + 
              ' Train Acc: ' + str(train_acc_array_epoch[epoch]))
        
        
        if train_params['fine_tune']==True:
            plot_path  = os.path.join(outdir, 'acc_vs_epoch_' \
                                + train_params['mode'] + '_fine_tuned.pdf')
        else:
            plot_path = os.path.join(outdir, 'acc_vs_epoch_' \
                                + train_params['mode'] + '.pdf')

        plotter.write_accuracy_to_file(start_epoch, epoch, 
                                       train_acc_array_epoch,
                                       train_params['fine_tune'],
                                       train_accuracy_txtfile)
        plotter.write_accuracy_to_file(start_epoch, epoch, 
                                       val_acc_array_epoch,
                                       train_params['fine_tune'],
                                       val_accuracy_txtfile)
        plotter.plot_accuracies(xdata=np.arange(0, epoch + 1) + 1,
                                ydata_train=train_acc_array_epoch[0:epoch + 1], 
                                ydata_val=val_acc_array_epoch[0:epoch + 1], 
                                xlim=[1, max_epoch], ylim=[0, 1.0], 
                                savePath=plot_path)

        save_path = model_saver.save(sess, 
                                     os.path.join(outdir, 'ans_classifier_' + \
                                     train_params['mode']), global_step=epoch)

    sess.close()
    tf.reset_default_graph()
    
if __name__=='__main__':
    print 'Hello'
