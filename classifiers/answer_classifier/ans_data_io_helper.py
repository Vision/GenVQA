import json
import sys
import os
import time
import random
import pdb
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np
import tensorflow as tf
from scipy import misc
from collections import namedtuple
import region_ranker.perfect_ranker as region_proposer


qa_tuple = namedtuple('qa_tuple','image_id question answer')

num_proposals = 22
region_coords, region_coords_ = region_proposer.get_region_coords(75, 75)


def create_ans_dict():
    ans_dict = {
        'yes'      : 0,
        'no'       : 1,
        'red'      : 2,
        'green'    : 3,
        'blue'     : 4,
        'circle'   : 5,
        'square'   : 6,
        'triangle' : 7,
    }
    
    for i in range(0,10):
        ans_dict[str(i)] = 8+i
        
    inv_ans_dict = {v: k for k, v in ans_dict.items()}

    return ans_dict, inv_ans_dict


def parse_qa_anno(json_filename):
    with open(json_filename,'r') as json_file:
        raw_data = json.load(json_file)

    qa_dict = dict()
    for entry in raw_data:
        qa_dict[entry['question_id']] = qa_tuple(image_id = entry['image_id'],
                                                 question = entry['question'],
                                                 answer = entry['answer'])
    return qa_dict
                                                 

def read_parsed_questions(json_filename):
    with open(json_filename, 'r') as json_file:
        raw_data = json.load(json_file)

    parsed_q_dict = dict()
    for entry in raw_data:
        parsed_q_dict[entry['question_id']] = entry['question_parse']

    return parsed_q_dict


def get_vocab(qa_dict):
    vocab = dict()
    count = 0;
    for key, value in qa_dict.items():
        for word in value.question[0:-1].split():
            if word.lower() not in vocab:
                vocab[word.lower()] = count
                count = count + 1

        if value.answer.lower() not in vocab:
            vocab[value.answer.lower()] = count
            count = count + 1

    vocab['unk'] = count
    inv_vocab = {v: k for k, v in vocab.items()}

    return vocab, inv_vocab


def join_vocab(vocab, ans_vocab):
    joint_vocab = vocab.copy()
    count = len(joint_vocab)
    for word in ans_vocab.keys():
        if word not in joint_vocab:
            joint_vocab[word] = count
            count += 1

    return joint_vocab


def save_regions(image_dir, out_dir, qa_dict, region_anno_dict, start_id, 
                 batch_size, img_width, img_height):
    
    print('Saving Regions ...')
    if not os.path.exists(out_dir):
        os.mkdir(out_dir)

    region_shape = np.array([img_height/3, img_width/3], np.int32)

    image_done = dict()
    for i in xrange(start_id, start_id + batch_size):
        image_id = qa_dict[i].image_id
        image_done[image_id] = False

    for i in xrange(start_id, start_id + batch_size):
        if i%1000==0:
            print('question_id: ' + str(i))
            print('image_id: ' + str(qa_dict[i].image_id))

        image_id = qa_dict[i].image_id
        if image_done[image_id]==False:
            gt_regions_for_image = region_anno_dict[image_id]
            image = mpimg.imread(os.path.join(image_dir,
                                              str(image_id) + '.jpg'))
            resized_image = misc.imresize(image, (img_height, img_width))
            regions = region_proposer.rank_regions(resized_image, None, 
                                                   region_coords, 
                                                   region_coords_,
                                                   gt_regions_for_image)

            for j in xrange(num_proposals):
                filename = os.path.join(out_dir, '{}_{}.png'.format(image_id,j))
                resized_region = misc.imresize(regions[j].image, 
                                               (region_shape[0], 
                                                region_shape[1]))
                misc.imsave(filename,resized_region)
        
            image_done[image_id] = True

class batch_creator():
    def __init__(self, start_id, end_id):
        self.start_id = start_id
        self.end_id = end_id
        self.id_list = range(start_id, end_id+1)

    def shuffle_ids(self):
        random.shuffle(self.id_list)

    def qa_index(self, start_index, batch_size):
        return self.id_list[start_index - self.start_id 
                            : start_index - self.start_id + batch_size]

    def ans_mini_batch_loader(self, qa_dict, region_anno_dict, ans_dict, vocab, 
                              image_dir, mean_image, start_index, batch_size, 
                              parsed_q_dict, img_height=100, img_width=100, 
                              channels = 3):

        q_ids = self.qa_index(start_index, batch_size)

        ans_labels = np.zeros(shape=[batch_size, len(ans_dict)])
        for i in xrange(batch_size):
            q_id = q_ids[i]
            answer = qa_dict[q_id].answer
            ans_labels[i, ans_dict[answer]] = 1
            
        # number of regions in the batch
        count = batch_size*num_proposals;
        region_shape = np.array([img_height/3, img_width/3], np.int32)
        region_images = np.zeros(shape=[count, region_shape[0], 
                                        region_shape[1], channels])
        region_score = np.zeros(shape=[1,count])
        partition = np.zeros(shape=[count])
        parsed_q = dict()

        for i in xrange(batch_size):
            q_id = q_ids[i]
            image_id = qa_dict[q_id].image_id
            question = qa_dict[q_id].question
            answer = qa_dict[q_id].answer
            gt_regions_for_image = region_anno_dict[image_id]
            regions = region_proposer.rank_regions(None, question,
                                                   region_coords, 
                                                   region_coords_,
                                                   gt_regions_for_image,
                                                   False)
            
            for j in xrange(num_proposals):
                counter = j + i*num_proposals
                parsed_q[counter] = parsed_q_dict[q_id]
                proposal = regions[j]
                resized_region = mpimg.imread(os.path.join(image_dir, 
                                             '{}_{}.png'.format(image_id,j)))
                region_images[counter,:,:,:] = (resized_region) \
                                               - mean_image
                region_score[0,counter] = proposal.score
                partition[counter] = i
                
            score_start_id = i*num_proposals
            region_score[0, score_start_id:score_start_id+num_proposals] /=\
                    np.sum(region_score[0,score_start_id
                                        : score_start_id+num_proposals])

        return region_images, ans_labels, parsed_q, \
            region_score, partition
       

    def reshape_score(self, region_score):
        num_cols = num_proposals
        num_rows = region_score.shape[1]/num_proposals
        assertion_str = 'Number of proposals and batch size do not match ' + \
                        'dimension of region_score'
        assert (num_cols*num_rows==region_score.shape[1]), assertion_str
        
        return np.reshape(region_score,[num_rows, num_cols],'C')

 
obj_labels = {
    0: 'blank',
    1: 'square',
    2: 'triangle',
    3: 'circle',
}   


atr_labels = {
    0: 'red', 
    1: 'green',
    2: 'blue',
    3: 'blank',
}


class FeedDictCreator():
    def __init__(self, region_images, parsed_q, 
             keep_prob, plholder_dict, vocab):
        self.plholder_dict = plholder_dict
        self.parsed_q = parsed_q
        self.vocab = vocab
        self.max_words = 5
        self.feed_dict = {
            plholder_dict['image_regions']: region_images,
            plholder_dict['keep_prob']: keep_prob,
        }
        self.add_bin('bin0')
        self.add_bin('bin1')
        self.add_bin('bin2')
        self.add_bin('bin3')
        for i in xrange(4):
            bin_name = 'bin' + str(i)
            self.label_bin_containment(bin_name, obj_labels, 'obj')
            self.label_bin_containment(bin_name, atr_labels, 'atr')

    def add_bin(self, bin_name):
        num_q = len(self.parsed_q)
        shape_list = [num_q, len(self.vocab)]
        indices_list = []
        values_list = []
        for q_num in xrange(num_q):
            item = self.parsed_q[q_num]
            word_list = item[bin_name]
            num_words = len(word_list)
            assert_str = 'number of bin words exceeded limit'
            assert (num_words <= self.max_words), assert_str
            for word_num, word in enumerate(word_list):
                if word=='':
                    word = 'unk'
                indices_list.append((q_num, word_num))
                values_list.append(self.vocab[word.lower()])

        # convert to numpy arrays
        shape = np.asarray(shape_list)
        indices = np.asarray(indices_list)
        values = np.asarray(values_list)
        self.feed_dict[self.plholder_dict[bin_name + '_indices']] = indices
        self.feed_dict[self.plholder_dict[bin_name + '_values']] = values
        self.feed_dict[self.plholder_dict[bin_name + '_shape']] = shape

    def label_bin_containment(self, bin_name, labels, label_type):
        num_q = len(self.parsed_q)
        num_labels = len(labels)
        containment = np.zeros([num_q, num_labels], dtype='float32')
        for q_num in xrange(num_q):
            for i, label in labels.items():
                if label in [pq.lower() for pq in \
                             self.parsed_q[q_num][bin_name]]:
                    containment[q_num,i] = 1

        plholder = self.plholder_dict[bin_name + '_' + \
                                      label_type + '_' + 'cont']
        self.feed_dict[plholder] = containment


class RelFeedDictCreator(FeedDictCreator):
    def __init__(self, region_images, parsed_q, 
                 gt_region_scores, keep_prob, plholder_dict, vocab, is_train):
        FeedDictCreator.__init__(self, region_images, parsed_q,
                                 keep_prob, plholder_dict, vocab)
        self.feed_dict[plholder_dict['gt_scores']] = gt_region_scores
        self.feed_dict[plholder_dict['is_train']] = is_train
    

class AnsFeedDictCreator(FeedDictCreator):
    def __init__(self, region_images, ans_labels, parsed_q, 
                 region_scores, keep_prob, plholder_dict, vocab, is_train):
        FeedDictCreator.__init__(self, region_images, parsed_q,
                                 keep_prob, plholder_dict, vocab)
        self.feed_dict[plholder_dict['gt_answer']] = ans_labels
        self.feed_dict[plholder_dict['region_score']] = region_scores
        self.feed_dict[plholder_dict['is_train']] = is_train


class html_ans_table_writer():
    def __init__(self, filename):
        self.filename = filename
        self.html_file = open(self.filename, 'w')
        self.html_file.write("""<!DOCTYPE html>\n<html>\n<body>\n<table border="1" style="width:100%"> \n""")
    
    def add_element(self, col_dict):
        self.html_file.write('    <tr>\n')
        for key in range(len(col_dict)):
            self.html_file.write("""    <td>{}</td>\n""".format(col_dict[key]))
        self.html_file.write('    </tr>\n')

    def image_tag(self, image_path, height, width):
        return """<img src="{}" alt="IMAGE NOT FOUND!" height={} width={}>""".format(image_path,height,width)
        
    def close_file(self):
        self.html_file.write('</table>\n</body>\n</html>')
        self.html_file.close()

    
if __name__=='__main__':
    train_anno_filename = '/home/tanmay/Code/GenVQA/GenVQA/' + \
                          'shapes_dataset/train_anno.json'

    region_anno_filename = '/home/tanmay/Code/GenVQA/GenVQA/' + \
                           'shapes_dataset/regions_anno.json'

    image_dir = '/home/tanmay/Code/GenVQA/GenVQA/' + \
                'shapes_dataset/images'

    mean_image = np.load('/home/tanmay/Code/GenVQA/Exp_Results/' + \
                         'Obj_Classifier/mean_image.npy')

    qa_anno_dict = parse_qa_anno(train_anno_filename)
    region_anno_dict = region_proposer.parse_region_anno(region_anno_filename)
    ans_dict, _ = create_ans_dict()
    vocab, _ = get_vocab(qa_anno_dict)
    
    
    region_images, ans_labels, question_encodings, score, partition = \
        ans_mini_batch_loader(qa_anno_dict, region_anno_dict, ans_dict, vocab,
                              image_dir, mean_image, 1, 1, 25, 25, 3)

    print(ans_labels.shape)
    print(question_encodings.shape)
    print(region_images.shape)
    print(score)
    print(partition)
    
