import sys
import os
import json
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np
import math
import random
import pdb
import tensorflow as tf
import tf_graph_creation_helper as graph_creator
import plot_helper as plotter
import ans_data_io_helper as ans_io_helper
import region_ranker.perfect_ranker as region_proposer 
import train_ans_classifier as ans_trainer
from PIL import Image, ImageDraw

def get_pred(y, qa_anno_dict, region_anno_dict, parsed_q_dict, ans_vocab, vocab,
             image_dir, mean_image, start_index, val_set_size, batch_size,
             plholder_dict, img_height, img_width, batch_creator):

    inv_ans_vocab = {v: k for k, v in ans_vocab.items()}
    pred_list = []
    correct = 0
    max_iter = int(math.ceil(val_set_size*1.0/batch_size))
    batch_size_tmp = batch_size
    for i in xrange(max_iter):
        if i==(max_iter-1):
            batch_size_tmp = val_set_size - i*batch_size

        print('Iter: ' + str(i+1) + '/' + str(max_iter))

        region_images, ans_labels, parsed_q, \
        region_score, partition = batch_creator \
            .ans_mini_batch_loader(qa_anno_dict, 
                                   region_anno_dict, 
                                   ans_vocab, vocab, 
                                   image_dir, mean_image, 
                                   start_index+i*batch_size, 
                                   batch_size_tmp, parsed_q_dict,
                                   img_height, img_width, 3)
            
        if i==max_iter-1:
                                    
            residual_batch_size = batch_size - batch_size_tmp
            residual_regions = residual_batch_size*ans_io_helper.num_proposals

            residual_region_images = np.zeros(shape=[residual_regions,
                                                     img_height/3, img_width/3, 
                                                     3])
            # residual_questions = np.zeros(shape=[residual_regions, 
            #                                      len(vocab)])
            
            residual_ans_labels = np.zeros(shape=[residual_batch_size, 
                                                  len(ans_vocab)])
            residual_region_score = np.zeros(shape=[1, residual_regions])

            region_images = np.concatenate((region_images, 
                                            residual_region_images),
                                           axis=0)
#            questions = np.concatenate((questions, residual_questions), axis=0)
            for k in xrange(batch_size_tmp*22, batch_size*22):
                parsed_q[k] = {
                    'bin0': [''],
                    'bin1': [''],
                    'bin2': [''],
                    'bin3': [''],
                }

            ans_labels = np.concatenate((ans_labels, residual_ans_labels), 
                                        axis=0)
            region_score = np.concatenate((region_score, residual_region_score),
                                          axis=1)

        
        feed_dict = ans_io_helper \
            .AnsFeedDictCreator(region_images, 
                                ans_labels, 
                                parsed_q,
                                region_score,
                                1.0, 
                                plholder_dict,
                                vocab).feed_dict

        ans_ids = np.argmax(y.eval(feed_dict), 1)
        for j in xrange(batch_size_tmp):
            pred_list = pred_list + [{
                'question_id' : start_index+i*batch_size+j,
                'answer' : inv_ans_vocab[ans_ids[j]]
            }]

    return pred_list

def eval(eval_params):
    sess = tf.InteractiveSession()
    
    train_anno_filename = eval_params['train_json']
    test_anno_filename = eval_params['test_json']
    parsed_q_filename = eval_params['parsed_q_json']
    regions_anno_filename = eval_params['regions_json']
    image_regions_dir = eval_params['image_regions_dir']
    outdir = eval_params['outdir']
    model = eval_params['model']
    batch_size = eval_params['batch_size']
    test_start_id = eval_params['test_start_id']
    test_set_size = eval_params['test_set_size']
    if not os.path.exists(outdir):
        os.mkdir(outdir)

    qa_anno_dict_train = ans_io_helper.parse_qa_anno(train_anno_filename)
    qa_anno_dict = ans_io_helper.parse_qa_anno(test_anno_filename)
    parsed_q_dict = ans_io_helper.read_parsed_questions(parsed_q_filename)
    region_anno_dict = region_proposer.parse_region_anno(regions_anno_filename)
    ans_vocab, inv_ans_vocab = ans_io_helper.create_ans_dict()
    vocab, inv_vocab = ans_io_helper.get_vocab(qa_anno_dict_train)

    # Create graph
    g = tf.get_default_graph()
    plholder_dict = graph_creator.placeholder_inputs_ans(len(vocab), 
                                                         len(ans_vocab), 
                                                         mode='gt')

    image_regions = plholder_dict['image_regions']
    questions = plholder_dict['questions']
    keep_prob = plholder_dict['keep_prob']
    y = plholder_dict['gt_answer']
    region_score = plholder_dict['region_score']

    y_pred_obj = graph_creator.obj_comp_graph(image_regions, 1.0)
    obj_feat_op = g.get_operation_by_name('obj/conv2/obj_feat')
    obj_feat = obj_feat_op.outputs[0]
    y_pred_atr = graph_creator.atr_comp_graph(image_regions, 1.0, obj_feat)
    atr_feat_op = g.get_operation_by_name('atr/conv2/atr_feat')
    atr_feat = atr_feat_op.outputs[0]
    pred_rel_score = graph_creator.rel_comp_graph(plholder_dict,
                                                  obj_feat, atr_feat,
                                                  y_pred_obj, y_pred_atr,
                                                  'q_obj_atr_reg',
                                                  1.0, len(vocab), batch_size) 
    y_pred = graph_creator.ans_comp_margin_graph(plholder_dict, 
                                                 obj_feat, atr_feat, 
                                                 y_pred_obj, y_pred_atr,
                                                 vocab, inv_vocab, ans_vocab, 
                                                 eval_params['mode'])
    pred_rel_score_vec = tf.reshape(pred_rel_score, 
                                    [1, batch_size*ans_io_helper.num_proposals])

    y_avg = graph_creator.aggregate_y_pred(y_pred, pred_rel_score_vec, 
                                           batch_size,  
                                           ans_io_helper.num_proposals, 
                                           len(ans_vocab))
    
    accuracy = graph_creator.evaluation(y, y_avg)

    # Collect variables
    rel_vars = tf.get_collection(tf.GraphKeys.VARIABLES, scope='rel')
    obj_vars = tf.get_collection(tf.GraphKeys.VARIABLES, scope='obj')
    atr_vars = tf.get_collection(tf.GraphKeys.VARIABLES, scope='atr')
    pretrained_vars, vars_to_train, vars_to_restore, vars_to_save, \
        vars_to_init, vars_dict = ans_trainer \
            .get_process_flow_vars(eval_params['mode'], 
                                   obj_vars, atr_vars, rel_vars,
                                   True)

    # Restore model
    restorer = tf.train.Saver(vars_to_restore)
    if os.path.exists(model):
        restorer.restore(sess, model)
    else:
        print 'Failed to read model from file ' + model

    sess.run(tf.initialize_variables(vars_to_init))

    mean_image = np.load('/home/tanmay/Code/GenVQA/Exp_Results/' + \
                         'Obj_Classifier/mean_image.npy')

    # Batch creator
    test_batch_creator = ans_io_helper.batch_creator(test_start_id,
                                                     test_start_id 
                                                     + test_set_size - 1)
    # Get predictions
    pred_dict = get_pred(y_avg, qa_anno_dict, region_anno_dict, 
                         parsed_q_dict, ans_vocab, 
                         vocab, image_regions_dir, mean_image, test_start_id, 
                         test_set_size, batch_size, plholder_dict, 75, 75,
                         test_batch_creator)

    json_filename = os.path.join(outdir, 'predicted_ans_' + \
                                 eval_params['mode'] + '.json')
    with open(json_filename,'w') as json_file:
        json.dump(pred_dict, json_file)

    
def create_html_file(outdir, test_anno_filename, regions_anno_filename,
                     pred_json_filename, image_dir, num_pred_to_display, mode):
    qa_dict = ans_io_helper.parse_qa_anno(test_anno_filename)
    region_anno_dict = region_proposer.parse_region_anno(regions_anno_filename)
    ans_vocab, inv_ans_vocab = ans_io_helper.create_ans_dict()

    with open(pred_json_filename,'r') as json_file:
        raw_data = json.load(json_file)
    
    # Create director for storing images with region boxes
    images_bbox_dir = os.path.join(outdir, 'images_bbox' + '_' + mode)
    if not os.path.exists(images_bbox_dir):
        os.mkdir(images_bbox_dir)
    
    col_dict = {
        0 : 'Question_Id',
        1 : 'Question',
        2 : 'Answer (GT)',
        3 : 'Answer (Pred)',
        4 : 'Image',
    }
    html_correct_filename = os.path.join(outdir, 
                                         'correct_ans_' + mode + '.html')
    html_writer_correct = ans_io_helper \
        .html_ans_table_writer(html_correct_filename)
    html_writer_correct.add_element(col_dict)

    html_incorrect_filename = os.path.join(outdir, 
                                           'incorrect_ans_' + mode + '.html')
    html_writer_incorrect = ans_io_helper \
        .html_ans_table_writer(html_incorrect_filename)
    html_writer_incorrect.add_element(col_dict)

    region_coords, region_coords_ = region_proposer.get_region_coords(300,300)
    
    random.shuffle(raw_data)

    count = 0
    for entry in raw_data:
        if count == num_pred_to_display:
            break
        q_id = entry['question_id']
        pred_ans = entry['answer']
        gt_ans = qa_dict[q_id].answer
        question = qa_dict[q_id].question
        img_id = qa_dict[q_id].image_id
        image_filename = os.path.join(image_dir, str(img_id) + '.jpg')
        image = Image.open(image_filename)
        
        regions = region_proposer.rank_regions(image, question, region_coords, 
                                               region_coords_, 
                                               region_anno_dict[img_id],
                                               crop=False)
        dr = ImageDraw.Draw(image)
        # print(q_id)
        # print([regions[key].score for key in regions.keys()])
        for i in xrange(ans_io_helper.num_proposals):
            if not regions[i].score==0:
                coord = regions[i].coord
                x1 = coord[0]
                y1 = coord[1]
                x2 = coord[2]
                y2 = coord[3]
                dr.rectangle([(x1,y1),(x2,y2)], outline="red")
        
        image_bbox_filename = os.path.join(images_bbox_dir,str(q_id) + '.jpg')
        image.save(image_bbox_filename)
        image_bbox_filename_rel = 'images_bbox_'+ mode +'/'+ str(q_id) + '.jpg' 
        col_dict = {
            0 : q_id,
            1 : question,
            2 : gt_ans,
            3 : pred_ans,
            4 : html_writer_correct.image_tag(image_bbox_filename_rel,50,50)
        }
        if pred_ans==gt_ans:
            html_writer_correct.add_element(col_dict)
        else:
            html_writer_incorrect.add_element(col_dict)

        count += 1

    html_writer_correct.close_file()
    html_writer_incorrect.close_file()
    

if __name__=='__main__':
    mode = 'q_obj_atr'
    model_num = 9
    ans_classifier_eval_params = {
        'train_json': '/home/tanmay/Code/GenVQA/GenVQA/shapes_dataset/train_anno.json',
        'test_json': '/home/tanmay/Code/GenVQA/GenVQA/shapes_dataset/test_anno.json',
        'regions_json': '/home/tanmay/Code/GenVQA/GenVQA/shapes_dataset/regions_anno.json',
        'parsed_q_json': '/home/tanmay/Code/GenVQA/GenVQA/shapes_dataset/parsed_questions.json',
        'image_dir': '/home/tanmay/Code/GenVQA/GenVQA/shapes_dataset/images',
        'image_regions_dir': '/mnt/ramdisk/image_regions',
        'outdir': '/home/tanmay/Code/GenVQA/Exp_Results/Ans_Classifier_Margin',
        'rel_model': '/home/tanmay/Code/GenVQA/Exp_Results/Rel_Classifier_Obj_Atr_Explt/rel_classifier_q_obj_atr_reg_explt-9',
        'model': '/home/tanmay/Code/GenVQA/Exp_Results/Ans_Classifier_Margin/ans_classifier_' + mode + '-' + str(model_num),
        'mode' : mode,
        'batch_size': 20,
        'test_start_id': 94645,
        'test_set_size': 143495-94645+1,
    }
    
    eval(ans_classifier_eval_params)
    outdir = ans_classifier_eval_params['outdir']
    test_anno_filename = ans_classifier_eval_params['test_json']
    regions_anno_filename = ans_classifier_eval_params['regions_json']
    pred_json_filename = os.path.join(outdir, 'predicted_ans_'+ mode  +'.json')
    image_dir = ans_classifier_eval_params['image_dir']
    create_html_file(outdir, test_anno_filename, regions_anno_filename,
                      pred_json_filename, image_dir, 1000, mode)
