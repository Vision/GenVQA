import sys
import json
import os
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np
from scipy import misc
import tensorflow as tf
import obj_data_io_helper as shape_data_loader
import tf_graph_creation_helper as graph_creator
import plot_helper as plotter

def eval(eval_params):
    sess = tf.InteractiveSession()
    
    x, y, keep_prob = graph_creator.placeholder_inputs()
    y_pred = graph_creator.obj_comp_graph(x, keep_prob)
    accuracy = graph_creator.evaluation(y, y_pred)

    # Collect variables
    vars_to_restore = tf.get_collection(tf.GraphKeys.VARIABLES, scope='obj')
    print('Variables to restore:')
    print([var.name for var in vars_to_restore])

    saver = tf.train.Saver(vars_to_restore)
    saver.restore(sess, eval_params['model_name'] + '-' + \
                  str(eval_params['global_step']))

    print 'Loading mean image'
    mean_image = np.load(os.path.join(eval_params['out_dir'], 'mean_image.npy'))
    test_json_filename = eval_params['test_json']
    with open(test_json_filename, 'r') as json_file: 
        raw_json_data = json.load(json_file)
        test_json_data = dict()
        for entry in raw_json_data:
            if entry['image_id'] not in test_json_data:
                test_json_data[entry['image_id']]=entry['config']

    image_dir = eval_params['image_dir']
    html_dir = eval_params['html_dir']
    if not os.path.exists(html_dir):
        os.mkdir(html_dir)

    html_writer = shape_data_loader \
        .html_obj_table_writer(os.path.join(html_dir, 'index.html'))

    col_dict = {
        0: 'Grount Truth',
        1: 'Prediction',
        2: 'Image'}

    html_writer.add_element(col_dict)

    shape_dict = {
        0: 'blank',
        1: 'square',
        2: 'triangle',
        3: 'circle'}

    batch_size = 100
    correct = 0
    for i in range(50):
        test_batch = shape_data_loader.obj_mini_batch_loader(test_json_data, 
            image_dir, mean_image, 10000 + i * batch_size, batch_size, 75, 75)
        feed_dict_test = {x: test_batch[0], y: test_batch[1], keep_prob: 1.0}
        result = sess.run([accuracy, y_pred], feed_dict=feed_dict_test)
        correct = correct + result[0] * batch_size
        print correct

        for row in range(batch_size * 9):
            gt_id = np.argmax(test_batch[1][row, :])
            pred_id = np.argmax(result[1][row, :])

            if not gt_id == pred_id:
                img_filename = os.path.join(html_dir, 
                                            '{}_{}.png'.format(i, row))
                misc.imsave(img_filename, 
                            test_batch[0][row, :, :, :] + mean_image)
                col_dict = {
                    0: shape_dict[gt_id],
                    1: shape_dict[pred_id],
                    2: html_writer.image_tag('{}_{}.png' \
                                             .format(i, row), 25, 25)}
                html_writer.add_element(col_dict)

    html_writer.close_file()
    print 'Test Accuracy: {}'.format(correct / 5000)

    sess.close()
    tf.reset_default_graph()
