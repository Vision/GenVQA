import sys
import os
import json
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np
import tensorflow as tf
import object_classifiers.obj_data_io_helper as shape_data_loader
import tf_graph_creation_helper as graph_creator
import plot_helper as plotter

def train(train_params):
    sess = tf.InteractiveSession()

    x, y, keep_prob = graph_creator.placeholder_inputs()
    y_pred = graph_creator.obj_comp_graph(x, keep_prob)
    cross_entropy = graph_creator.loss(y, y_pred)
    W_fc1 = graph_creator.get_variable('obj/fc1/W')
    conv_varnames_list = [
        'obj/conv1/W',
        'obj/conv2/W',
    ]
    conv_params_dict = graph_creator.get_list_of_variables(conv_varnames_list)
    conv_params_list = [conv_params_dict['obj/conv1/W'], 
                        conv_params_dict['obj/conv2/W']]
    regularizer_filters = graph_creator.regularize_params(conv_params_list)
    regularizer_fc1 = graph_creator.regularize_params([W_fc1])
    total_loss = cross_entropy + 1e-1 * regularizer_fc1 + \
                 1e-3 * regularizer_filters
    
    accuracy = graph_creator.evaluation(y, y_pred)

    outdir = train_params['out_dir']
    if not os.path.exists(outdir):
        os.mkdir(outdir)

    # Training Data
    img_width = 75
    img_height = 75
    train_json_filename = train_params['train_json']
    with open(train_json_filename, 'r') as json_file: 
        raw_json_data = json.load(json_file)
        train_json_data = dict()
        for entry in raw_json_data:
            if entry['image_id'] not in train_json_data:
                train_json_data[entry['image_id']]=entry['config']


    image_dir = train_params['image_dir']
    if train_params['mean_image']=='':
        print('Computing mean image')
        mean_image = shape_data_loader.mean_image(train_json_data, 
                                                  image_dir, 1000, 100, 
                                                  img_height, img_width)
    else:
        print('Loading mean image')
        mean_image = np.load(train_params['mean_image'])
    np.save(os.path.join(outdir, 'mean_image.npy'), mean_image)

    # Val Data
    val_batch = shape_data_loader.obj_mini_batch_loader(train_json_data, 
                                                        image_dir, mean_image, 
                                                        9501, 499, 
                                                        img_height, img_width)
    feed_dict_val = {x: val_batch[0], y: val_batch[1], keep_prob: 1.0}
    
    # Collect variables
    all_vars = tf.get_collection(tf.GraphKeys.VARIABLES)
    vars_to_save = tf.get_collection(tf.GraphKeys.VARIABLES, scope='obj')

    print('All variables:')
    print([var.name for var in all_vars])
    print('Variables to save:')
    print([var.name for var in vars_to_save])

    # Session Saver
    saver = tf.train.Saver(vars_to_save)
    
    # Add optimization op
    train_step = tf.train.AdamOptimizer(train_params['adam_lr']) \
                         .minimize(total_loss)
    # Start Training
    sess.run(tf.initialize_all_variables())
    batch_size = 10
    max_epoch = 2
    max_iter = 950
    val_acc_array_iter = np.empty([max_iter * max_epoch])
    val_acc_array_epoch = np.zeros([max_epoch])
    train_acc_array_epoch = np.zeros([max_epoch])
    for epoch in range(max_epoch):
        for i in range(max_iter):
            if i%100==0:
                print('Iter: ' + str(i))
                print('Val Acc: ' + str(accuracy.eval(feed_dict_val)))

            train_batch = shape_data_loader \
                .obj_mini_batch_loader(train_json_data, image_dir, mean_image, 
                                       1 + i * batch_size, batch_size, 
                                       img_height, img_width)
            feed_dict_train = {
                x: train_batch[0], 
                y: train_batch[1], 
                keep_prob: 0.5
            }

            _, current_train_batch_acc = sess.run([train_step, accuracy], 
                                                  feed_dict=feed_dict_train)
            train_acc_array_epoch[epoch] = train_acc_array_epoch[epoch] \
                                           + current_train_batch_acc

        train_acc_array_epoch[epoch] = train_acc_array_epoch[epoch] / max_iter
        val_acc_array_epoch[epoch] = accuracy.eval(feed_dict_val)
        
        plotter.plot_accuracies(xdata=np.arange(0, epoch + 1) + 1, 
                                ydata_train=train_acc_array_epoch[0:epoch + 1], 
                                ydata_val=val_acc_array_epoch[0:epoch + 1],
                                xlim=[1, max_epoch], ylim=[0, 1.0], 
                                savePath=os.path.join(outdir, 
                                                      'acc_vs_epoch.pdf'))
        save_path = saver.save(sess, os.path.join(outdir, 'obj_classifier'), 
                               global_step=epoch)

    sess.close()
    tf.reset_default_graph()

if __name__ == '__main__':
    train()
