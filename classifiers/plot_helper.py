#Embedded file name: /home/tanmay/Code/GenVQA/GenVQA/classifiers/plot_helper.py
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np
import tensorflow as tf

def write_accuracy_to_file(start_epoch, current_epoch, accuracy, 
                           fine_tune, filename):
    # if fine_tune==True:
    #     txtfile = open(filename, 'a')
    # else:
    #     txtfile = open(filename, 'w')
        
    print 'Appending to existing file or creating file if does not exist -> ' \
        + filename
    txtfile = open(filename, 'a+')

    for epoch in xrange(current_epoch, current_epoch+1):
        txtfile.write('iter: ' + str(epoch) + \
                      ' accuracy: ' + str(accuracy[epoch]) + '\n')
    
    txtfile.close()

def plot_accuracy(xdata, ydata, xlim = None, ylim = None, savePath = None):
    fig, ax = plt.subplots(nrows=1, ncols=1)
    ax.plot(xdata, ydata)
    plt.xlabel('Iterations')
    plt.ylabel('Accuracy')
    if not xlim == None:
        plt.xlim(xlim)
    if not ylim == None:
        plt.ylim(ylim)
    if not savePath == None:
        fig.savefig(savePath)
    plt.close(fig)


def plot_accuracies(xdata, ydata_train, ydata_val, xlim = None, ylim = None, savePath = None):
    fig, ax = plt.subplots(nrows=1, ncols=1)
    ax.plot(xdata, ydata_train, xdata, ydata_val)
    plt.xlabel('Epochs')
    plt.ylabel('Accuracy')
    plt.legend(['Train', 'Val'], loc='lower right')
    if not xlim == None:
        plt.xlim(xlim)
    if not ylim == None:
        plt.ylim(ylim)
    if not savePath == None:
        fig.savefig(savePath)
    plt.close(fig)
