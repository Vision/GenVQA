import numpy as np
import math
import pdb
import tensorflow as tf
import answer_classifier.ans_data_io_helper as ans_io_helper

graph_config = {
    'num_objects': 4,
    'num_attributes': 4,
    'obj_feat_dim': 392,
    'atr_feat_dim': 392,
    'region_feat_dim': 392, #3136
    'word_vec_dim': 50,
    'q_embed_dim': 200,
    'ans_fc1_dim': 300,
    'rel_fc1_dim': 100,
    'joint_embed_dim': 100,
}

def get_variable(var_scope):
    var_list = tf.get_collection(tf.GraphKeys.VARIABLES, scope=var_scope)
    
    assert_str = 'No variable exists by that name: ' + var_scope
    assert len(var_list)!=0, assert_str
    assert_str = 'Multiple variables exist by that name: ' + var_scope
    assert len(var_list)==1, assert_str
    
    return var_list[0]


def get_list_of_variables(var_scope_list):
    var_dict = dict()
    for var_scope in var_scope_list:
        var_dict[var_scope] = get_variable(var_scope)
    return var_dict


def weight_variable(tensor_shape, fan_in=None, var_name='W'):
    if fan_in==None:
        fan_in = reduce(lambda x, y: x*y, tensor_shape[0:-1])

    stddev = math.sqrt(2.0/fan_in)
    initial = tf.truncated_normal(shape=tensor_shape, mean=0.0, stddev=stddev)
    return tf.Variable(initial_value=initial, name=var_name)


def bias_variable(tensor_shape, var_name='b'):
    initial = tf.constant(value=0.0, shape=tensor_shape)
    return tf.Variable(initial_value=initial, name=var_name)


def conv2d(x, W, var_name = 'conv'):
    return tf.nn.conv2d(x, W, strides=[1,1,1,1], padding='SAME', name=var_name)


def max_pool_2x2(x, var_name = 'h_pool'):
    return tf.nn.max_pool(x, ksize=[1,2,2,1], strides=[1,2,2,1], 
                          padding='SAME', name=var_name)


def placeholder_inputs(mode = 'gt'):
    x = tf.placeholder(tf.float32, shape=[None,25,25,3])
    keep_prob = tf.placeholder(tf.float32)
    if mode == 'gt':
        print 'Creating placeholder for ground truth'
        y = tf.placeholder(tf.float32, shape=[None, 4])
        return (x, y, keep_prob)
    if mode == 'no_gt':
        print 'No placeholder for ground truth'
        return (x, keep_prob)
    

def placeholder_inputs_rel(num_proposals, total_vocab_size, mode = 'gt'):
    plholder_dict = {
        'image_regions': tf.placeholder(tf.float32, [None,25,25,3], 
                                        'image_regions'),
        'keep_prob': tf.placeholder(tf.float32, name='keep_prob'),
        'is_train': tf.placeholder(tf.bool, [], 'is_train'),
    }
    for i in xrange(4):
        bin_name = 'bin' + str(i)
        plholder_dict[bin_name + '_shape'] = \
            tf.placeholder(tf.int64, [2], bin_name + '_shape')
        plholder_dict[bin_name + '_indices'] = \
            tf.placeholder(tf.int64, [None, 2], bin_name + '_indices')
        plholder_dict[bin_name + '_values'] = \
            tf.placeholder(tf.int64, [None], bin_name + '_values')
        plholder_dict[bin_name + '_obj_cont'] = \
            tf.placeholder(tf.float32, [None, graph_config['num_objects']],
                           bin_name + '_obj_cont')
        plholder_dict[bin_name + '_atr_cont'] = \
            tf.placeholder(tf.float32, [None, graph_config['num_attributes']],
                           bin_name + '_atr_cont')
    if mode == 'gt':
        print 'Creating placeholder for ground truth'
        plholder_dict['gt_scores'] = tf.placeholder(tf.float32,\
            shape=[None, ans_io_helper.num_proposals], name = 'gt_scores')
        return plholder_dict
    if mode == 'no_gt':
        print 'No placeholder for ground truth'
        return plholder_dict


def placeholder_inputs_ans(total_vocab_size, ans_vocab_size, mode='gt'):
    plholder_dict = {
        'image_regions': tf.placeholder(tf.float32, [None,25,25,3], 
                                        'image_regions'),
        'keep_prob': tf.placeholder(tf.float32, name='keep_prob'),
        'questions': tf.placeholder(tf.float32, [None,total_vocab_size], 
                                    'questions'),
        'region_score': tf.placeholder(tf.float32, [1,None], 
                                       'region_score'),

        'is_train': tf.placeholder(tf.bool, [], 'is_train')
    }
    for i in xrange(4):
        bin_name = 'bin' + str(i)
        plholder_dict[bin_name + '_shape'] = \
            tf.placeholder(tf.int64, [2], bin_name + '_shape')
        plholder_dict[bin_name + '_indices'] = \
            tf.placeholder(tf.int64, [None, 2], bin_name + '_indices')
        plholder_dict[bin_name + '_values'] = \
            tf.placeholder(tf.int64, [None], bin_name + '_values')
        plholder_dict[bin_name + '_obj_cont'] = \
            tf.placeholder(tf.float32, [None, graph_config['num_objects']],
                           bin_name + '_obj_cont')
        plholder_dict[bin_name + '_atr_cont'] = \
            tf.placeholder(tf.float32, [None, graph_config['num_attributes']],
                           bin_name + '_atr_cont')
        
    if mode == 'gt':
        print 'Creating placeholder for ground truth'
        plholder_dict['gt_answer'] = tf.placeholder(tf.float32, 
                                                    shape=[None, 
                                                           ans_vocab_size],
                                                    name = 'gt_answer')
        return plholder_dict
    if mode == 'no_gt':
        print 'No placeholder for ground truth'
        return plholder_dict
        

def obj_comp_graph(x, keep_prob):
    with tf.name_scope('obj') as obj_graph:

        with tf.name_scope('conv1') as conv1:

            W_conv1 = weight_variable([5,5,3,4])
            b_conv1 = bias_variable([4])
            a_conv1 = tf.add(conv2d(x, W_conv1), b_conv1, name='a')
            h_conv1 = tf.nn.relu(a_conv1, name='h')
            h_pool1 = max_pool_2x2(h_conv1)
            h_conv1_drop = tf.nn.dropout(h_pool1, keep_prob, name='h_pool_drop')

        with tf.name_scope('conv2') as conv2:

            W_conv2 = weight_variable([3,3,4,8])
            b_conv2 = bias_variable([8])
            a_conv2 = tf.add(conv2d(h_pool1, W_conv2), b_conv2, name='a')
            h_conv2 = tf.nn.relu(a_conv2, name='h')
            h_pool2 = max_pool_2x2(h_conv2)
            h_pool2_drop = tf.nn.dropout(h_pool2, keep_prob, name='h_pool_drop')
            h_pool2_drop_shape = h_pool2_drop.get_shape()
            obj_feat_dim = reduce(lambda f, g: f*g, 
                                  [dim.value for dim in h_pool2_drop_shape[1:]])
            obj_feat = tf.reshape(h_pool2_drop, [-1, obj_feat_dim], 
                                           name='obj_feat')

            print('Object feature dimension: ' + str(obj_feat_dim)) #392

        with tf.name_scope('fc1') as fc1:

            W_fc1 = weight_variable([obj_feat_dim, graph_config['num_objects']])
            b_fc1 = bias_variable([4])
            logits = tf.add(tf.matmul(obj_feat, W_fc1), b_fc1, name='logits')
        
        y_pred = tf.nn.softmax(logits, name='softmax')

    return y_pred


def atr_comp_graph(x, keep_prob, obj_feat):
    with tf.name_scope('atr') as obj_graph:

        with tf.name_scope('conv1') as conv1:
            
            W_conv1 = weight_variable([5,5,3,4])
            b_conv1 = bias_variable([4])
            a_conv1 = tf.add(conv2d(x, W_conv1), b_conv1, name='a')
            h_conv1 = tf.nn.relu(a_conv1, name='h')
            h_pool1 = max_pool_2x2(h_conv1)
            h_conv1_drop = tf.nn.dropout(h_pool1, keep_prob, name='h_pool_drop')

        with tf.name_scope('conv2') as conv2:

            W_conv2 = weight_variable([3,3,4,8])
            b_conv2 = bias_variable([8])
            a_conv2 = tf.add(conv2d(h_pool1, W_conv2), b_conv2, name='a')
            h_conv2 = tf.nn.relu(a_conv2, name='h')
            h_pool2 = max_pool_2x2(h_conv2)
            h_pool2_drop = tf.nn.dropout(h_pool2, keep_prob, name='h_pool_drop')
            h_pool2_drop_shape = h_pool2_drop.get_shape()
            atr_feat_dim = reduce(lambda f, g: f*g, 
                                  [dim.value for dim in h_pool2_drop_shape[1:]])
            atr_feat = tf.reshape(h_pool2_drop, [-1, atr_feat_dim], 
                                  name='atr_feat')

            print('Attribute feature dimension: ' + str(atr_feat_dim)) #392

        with tf.name_scope('fc1') as fc1:

            W_obj_fc1 = weight_variable([graph_config['obj_feat_dim'], 
                                         graph_config['num_attributes']], 
                                        var_name='W_obj')
            W_atr_fc1 = weight_variable([atr_feat_dim, 
                                         graph_config['num_attributes']], 
                                        var_name='W_atr')
            b_fc1 = bias_variable([4])
            logits_atr = tf.matmul(atr_feat, W_atr_fc1, name='logits_atr')
            logits_obj = tf.matmul(obj_feat, W_obj_fc1, name='logits_obj')
            logits = 0.5*logits_atr + 0.5*logits_obj + b_fc1

        y_pred = tf.nn.softmax(logits, name='softmax')

    return y_pred


def q_bin_embed_graph(bin_name, word_vecs, plholder_dict):
    indices = plholder_dict[bin_name + '_indices']
    values = plholder_dict[bin_name + '_values']
    shape = plholder_dict[bin_name + '_shape']
    sp_ids = tf.SparseTensor(indices, values, shape)
    return tf.nn.embedding_lookup_sparse(word_vecs, sp_ids, None, 
                                         name=bin_name + '_embedding')


def explicit_feat_graph(bin_name, classifier_prob, 
                        classifier_type, plholder_dict):
    cont_plholder_name = bin_name + '_' + classifier_type + '_cont'
    feat_name = 'explt_' + bin_name + '_' + classifier_type
    dot_product = tf.mul(classifier_prob, plholder_dict[cont_plholder_name])
    return tf.reduce_mean(dot_product, 1, keep_dims=True, name=feat_name)


def rel_comp_graph(plholder_dict, obj_feat, atr_feat,
                   obj_prob, atr_prob, mode, keep_prob, 
                   vocab_size, batch_size):
    image_regions = plholder_dict['image_regions']
    with tf.name_scope('rel') as rel_graph:

        with tf.name_scope('word_embed') as q_embed:
            word_vecs = weight_variable([vocab_size,
                                         graph_config['word_vec_dim']],
                                        var_name='word_vecs')
            bin0_embed = q_bin_embed_graph('bin0', word_vecs, plholder_dict)
            bin1_embed = q_bin_embed_graph('bin1', word_vecs, plholder_dict)
            bin2_embed = q_bin_embed_graph('bin2', word_vecs, plholder_dict)
            bin3_embed = q_bin_embed_graph('bin3', word_vecs, plholder_dict)
            q_feat = tf.concat(1, [bin0_embed,
                                   bin1_embed,
                                   bin2_embed,
                                   bin3_embed], name='q_feat')
            
        with tf.name_scope('explicit_feat') as expl_feat:
            explt_feat_list = []
            for bin_num in xrange(4):
                bin_name = 'bin'+ str(bin_num)
                explt_feat_list.append(explicit_feat_graph(bin_name, obj_prob, 
                                        'obj', plholder_dict))
                explt_feat_list.append(explicit_feat_graph(bin_name, atr_prob, 
                                        'atr', plholder_dict))

            concat_explt_feat = tf.concat(1, explt_feat_list, 
                                          name = 'concat_explt_feat')
                                
            concat_explt_feat_dim = concat_explt_feat.get_shape()[1].value
            print('Concatenate explicit feature dimension: ' + \
                  str(concat_explt_feat_dim)) 

        with tf.name_scope('conv1') as conv1:
            W_conv1 = weight_variable([5,5,3,4])
            b_conv1 = bias_variable([4])
            a_conv1 = tf.add(conv2d(image_regions, W_conv1), b_conv1, name='a')
            h_conv1 = tf.nn.relu(a_conv1, name='h')
            h_pool1 = max_pool_2x2(h_conv1)
            h_conv1_drop = tf.nn.dropout(h_pool1, keep_prob, name='h_pool_drop')

        with tf.name_scope('conv2') as conv2:
            W_conv2 = weight_variable([3,3,4,8])
            b_conv2 = bias_variable([8])
            a_conv2 = tf.add(conv2d(h_pool1, W_conv2), b_conv2, name='a')
            h_conv2 = tf.nn.relu(a_conv2, name='h')
            h_pool2 = max_pool_2x2(h_conv2)
            h_pool2_drop = tf.nn.dropout(h_pool2, keep_prob, name='h_pool_drop')
            h_pool2_drop_shape = h_pool2_drop.get_shape()
            reg_feat_dim = reduce(lambda f, g: f*g, 
                                  [dim.value for dim in h_pool2_drop_shape[1:]])
            reg_feat = tf.reshape(h_pool2_drop, [-1, reg_feat_dim], 
                                  name='reg_feat')

            print('Relevance region feature dimension: ' + str(reg_feat_dim)) 

        with tf.name_scope('fc1') as fc1:
            fc1_dim = graph_config['rel_fc1_dim']
            obj_feat_shape = obj_feat.get_shape()
            obj_feat_dim = obj_feat_shape[1].value
            print 'Obj feat dim: {}'.format(obj_feat_dim)
            atr_feat_shape = atr_feat.get_shape()
            atr_feat_dim = atr_feat_shape[1].value
            print 'Atr feat dim: {}'.format(atr_feat_dim)
            W_reg_fc1 = weight_variable([reg_feat_dim, fc1_dim], 
                                        var_name='W_reg')
            W_q_fc1 = weight_variable([graph_config['q_embed_dim'], 
                                       fc1_dim], var_name='W_q')
            W_obj_fc1 = weight_variable([obj_feat_dim, 
                                         fc1_dim], var_name='W_obj')
            W_atr_fc1 = weight_variable([atr_feat_dim, 
                                         fc1_dim], var_name='W_atr')
            W_explt_fc1 = weight_variable([concat_explt_feat_dim,
                                           fc1_dim], var_name='W_explt')
            b_fc1 = bias_variable([fc1_dim])
            
            a_reg_fc1 = tf.matmul(reg_feat, W_reg_fc1, name='a_reg_fc1')
            a_q_fc1 = tf.matmul(q_feat, W_q_fc1, name='a_q_fc1')
            a_obj_fc1 = tf.matmul(obj_feat, W_obj_fc1, name='a_obj_fc1')
            a_atr_fc1 = tf.matmul(atr_feat, W_atr_fc1, name='a_atr_fc1')
            a_explt_fc1 = tf.matmul(concat_explt_feat, W_explt_fc1,
                                    name='a_explt_fc1')
            
            coeff = {
                'reg': 0.0,
                'q': 0.0,
                'obj': 0.0,
                'atr': 0.0,
                'explt': 0.0,
            }
            
            if mode=='q_reg_explt':
                print mode
                coeff['reg'] = 1/3.0
                coeff['q'] = 1/3.0
                coeff['explt'] = 1/3.0

            elif mode=='q_obj_atr_explt':
                print mode
                coeff['q'] = 0.1
                coeff['obj'] = 0.1
                coeff['atr'] = 0.1
                coeff['explt'] = 0.7

            elif mode=='q_obj_atr_reg_explt':
                print mode
                coeff['q'] = 0.05
                coeff['obj'] = 0.05
                coeff['atr'] = 0.05
                coeff['reg'] = 0.05
                coeff['explt'] = 0.8

            elif mode=='explt':
                coeff['explt'] = 1.0

            a_fc1 = coeff['reg']*a_reg_fc1 + coeff['q']*a_q_fc1 + \
                    coeff['obj']*a_obj_fc1 + coeff['atr']*a_atr_fc1 + \
                    coeff['explt']*a_explt_fc1 + b_fc1

            h_fc1 = tf.nn.relu(a_fc1, name='h')
            h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob, name='h_drop')

        with tf.name_scope('fc2') as fc2:
            W_fc2 = weight_variable([fc1_dim, 1])
            b_fc2 = bias_variable([1])
            
            vec_logits = tf.add(tf.matmul(h_fc1_drop, W_fc2), b_fc2, 
                                name='vec_logits')

            logits = tf.reshape(vec_logits,
                                [batch_size, ans_io_helper.num_proposals])
            
        y_pred = tf.nn.softmax(logits, name='softmax')

    return y_pred
   

def ans_comp_graph(plholder_dict, obj_feat, atr_feat, 
                   vocab, inv_vocab, ans_vocab_size, mode):
    vocab_size = len(vocab)
    image_regions = plholder_dict['image_regions']
    keep_prob = plholder_dict['keep_prob']

    with tf.name_scope('ans') as ans_graph:

        with tf.name_scope('word_embed') as word_embed:

            word_vecs = weight_variable([vocab_size,
                                         graph_config['word_vec_dim']],
                                        var_name='word_vecs')
            
            bin0_embed = q_bin_embed_graph('bin0', word_vecs, plholder_dict)
            bin1_embed = q_bin_embed_graph('bin1', word_vecs, plholder_dict)
            bin2_embed = q_bin_embed_graph('bin2', word_vecs, plholder_dict)
            bin3_embed = q_bin_embed_graph('bin3', word_vecs, plholder_dict)
            q_feat = tf.concat(1, [bin0_embed,
                                   bin1_embed,
                                   bin2_embed,
                                   bin3_embed], name='q_feat')
            
        with tf.name_scope('conv1') as conv1:
            num_filters_conv1 = 4
            W_conv1 = weight_variable([5,5,3,num_filters_conv1])
            b_conv1 = bias_variable([num_filters_conv1])
            a_conv1 = tf.add(conv2d(image_regions, W_conv1), b_conv1, name='a')
            h_conv1 = tf.nn.relu(a_conv1, name='h')
            h_pool1 = max_pool_2x2(h_conv1)
            h_conv1_drop = tf.nn.dropout(h_pool1, keep_prob, name='h_pool_drop')

        with tf.name_scope('conv2') as conv2:
            num_filters_conv2 = 8
            W_conv2 = weight_variable([3,3,num_filters_conv1,num_filters_conv2])
            b_conv2 = bias_variable([num_filters_conv2])
            a_conv2 = tf.add(conv2d(h_pool1, W_conv2), b_conv2, name='a')
            h_conv2 = tf.nn.relu(a_conv2, name='h')
            h_pool2 = max_pool_2x2(h_conv2)
            h_pool2_drop = tf.nn.dropout(h_pool2, keep_prob, name='h_pool_drop')
            h_pool2_drop_shape = h_pool2_drop.get_shape()
            region_feat_dim = reduce(lambda f, g: f*g, 
                                  [dim.value for dim in h_pool2_drop_shape[1:]])
            region_feat = tf.reshape(h_pool2_drop, [-1, region_feat_dim], 
                                     name='region_feat')

            print('Region feature dimension: ' + str(region_feat_dim)) #392
       
        with tf.name_scope('fc1') as fc1:

            fc1_dim = graph_config['ans_fc1_dim']
            W_region_fc1 = weight_variable([region_feat_dim, 
                                            fc1_dim], var_name='W_region')
            W_obj_fc1 = weight_variable([graph_config['obj_feat_dim'], 
                                         fc1_dim], var_name='W_obj')
            W_atr_fc1 = weight_variable([graph_config['atr_feat_dim'], 
                                         fc1_dim], var_name='W_atr')
            W_q_fc1 = weight_variable([graph_config['q_embed_dim'], 
                                       fc1_dim], var_name='W_q')
            b_fc1 = bias_variable([fc1_dim])

            a_fc1_region = tf.matmul(region_feat, W_region_fc1, 
                                      name='a_fc1_region')
            a_fc1_obj = tf.matmul(obj_feat, W_obj_fc1, name='a_fc1_obj') 
            a_fc1_atr = tf.matmul(atr_feat, W_atr_fc1, name='a_fc1_atr')
            a_fc1_q = tf.matmul(q_feat, W_q_fc1, name='a_fc1_q')
        
            coeff_reg = 0.0
            coeff_obj = 0.0
            coeff_atr = 0.0
            coeff_q = 0.0

            if mode=='q':
                coeff_q = 1.0

            elif mode=='q_reg':
                coeff_q = 0.5
                coeff_reg = 0.5

            elif mode=='q_obj_atr':
                coeff_q = 1/3.0
                coeff_obj = 1/3.0
                coeff_atr = 1/3.0

            elif mode=='q_obj_atr_reg':
                coeff_q = 1/4.0
                coeff_obj = 1/4.0
                coeff_atr = 1/4.0
                coeff_reg = 1/4.0

            a_fc1 = coeff_reg * a_fc1_region + \
                    coeff_obj * a_fc1_obj + \
                    coeff_atr * a_fc1_atr + \
                    coeff_q * a_fc1_q
            
            h_fc1 = tf.nn.relu(a_fc1, name='h')
            h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob, name='h_drop')

        with tf.name_scope('fc2') as fc2:
            W_fc2 = weight_variable([fc1_dim, ans_vocab_size])
            b_fc2 = bias_variable([ans_vocab_size])
            
            logits = tf.add(tf.matmul(h_fc1_drop, W_fc2), b_fc2, name='logits')

        y_pred = tf.nn.softmax(logits, name='softmax')

        return y_pred


def ans_comp_margin_graph(plholder_dict, obj_feat, atr_feat, obj_prob, atr_prob,
                          vocab, inv_vocab, ans_vocab, mode, train):
    vocab_size = len(vocab)
    image_regions = plholder_dict['image_regions']
    keep_prob = plholder_dict['keep_prob']
    ans_vocab_size = len(ans_vocab)
    
    inv_ans_vocab = {v:k for k, v in ans_vocab.items()}
    ans_in_vocab_ids_list = []
    for i in xrange(ans_vocab_size):
        ans_in_vocab_ids_list.append(vocab[inv_ans_vocab[i]])

    ans_in_vocab_ids_tensor = tf.constant(ans_in_vocab_ids_list, dtype=tf.int64)

    with tf.name_scope('ans') as ans_graph:

        with tf.name_scope('word_embed') as word_embed:

            word_vecs = weight_variable([vocab_size,
                                         graph_config['word_vec_dim']],
                                        var_name='word_vecs')
            
            bin0_embed = q_bin_embed_graph('bin0', word_vecs, plholder_dict)
            bin1_embed = q_bin_embed_graph('bin1', word_vecs, plholder_dict)
            bin2_embed = q_bin_embed_graph('bin2', word_vecs, plholder_dict)
            bin3_embed = q_bin_embed_graph('bin3', word_vecs, plholder_dict)
            q_feat = tf.concat(1, [bin0_embed,
                                   bin1_embed,
                                   bin2_embed,
                                   bin3_embed], name='q_feat')
            
            ans_embed = tf.nn.embedding_lookup(word_vecs, ans_in_vocab_ids_list,
                                               name='ans_embed')
            
        with tf.name_scope('explicit_feat') as expl_feat:
            explt_feat_list = []
            for bin_num in xrange(4):
                bin_name = 'bin'+ str(bin_num)
                explt_feat_list.append(explicit_feat_graph(bin_name, obj_prob, 
                                        'obj', plholder_dict))
                explt_feat_list.append(explicit_feat_graph(bin_name, atr_prob, 
                                        'atr', plholder_dict))

            concat_explt_feat = tf.concat(1, explt_feat_list, 
                                          name = 'concat_explt_feat')
                                
            concat_explt_feat_dim = concat_explt_feat.get_shape()[1].value
            print('Concatenate explicit feature dimension: ' + \
                  str(concat_explt_feat_dim))

        with tf.name_scope('conv1') as conv1:
            num_filters_conv1 = 4
            W_conv1 = weight_variable([5,5,3,num_filters_conv1])
            b_conv1 = bias_variable([num_filters_conv1])
            a_conv1 = tf.add(conv2d(image_regions, W_conv1), b_conv1, name='a')
            h_conv1 = tf.nn.relu(a_conv1, name='h')
            h_pool1 = max_pool_2x2(h_conv1)
            h_conv1_drop = tf.nn.dropout(h_pool1, keep_prob, name='h_pool_drop')

        with tf.name_scope('conv2') as conv2:
            num_filters_conv2 = 8
            W_conv2 = weight_variable([3,3,num_filters_conv1,num_filters_conv2])
            b_conv2 = bias_variable([num_filters_conv2])
            a_conv2 = tf.add(conv2d(h_pool1, W_conv2), b_conv2, name='a')
            h_conv2 = tf.nn.relu(a_conv2, name='h')
            h_pool2 = max_pool_2x2(h_conv2)
            h_pool2_drop = tf.nn.dropout(h_pool2, keep_prob, name='h_pool_drop')
            h_pool2_drop_shape = h_pool2_drop.get_shape()
            region_feat_dim = reduce(lambda f, g: f*g, 
                                  [dim.value for dim in h_pool2_drop_shape[1:]])
            region_feat = tf.reshape(h_pool2_drop, [-1, region_feat_dim], 
                                     name='region_feat')

            print('Region feature dimension: ' + str(region_feat_dim)) #392
       
        with tf.name_scope('fc1') as fc1:

            fc1_dim = graph_config['ans_fc1_dim']
            W_region_fc1 = weight_variable([region_feat_dim, 
                                            fc1_dim], var_name='W_region')
            W_obj_fc1 = weight_variable([graph_config['obj_feat_dim'], 
                                         fc1_dim], var_name='W_obj')
            W_atr_fc1 = weight_variable([graph_config['atr_feat_dim'], 
                                         fc1_dim], var_name='W_atr')
            W_q_fc1 = weight_variable([graph_config['q_embed_dim'], 
                                       fc1_dim], var_name='W_q')
            W_explt_fc1 = weight_variable([concat_explt_feat_dim,
                                           fc1_dim], var_name='W_explt')
            b_fc1 = bias_variable([fc1_dim])

            a_fc1_region = tf.matmul(region_feat, W_region_fc1, 
                                     name='a_fc1_region')
            a_fc1_obj = tf.matmul(obj_feat, W_obj_fc1, name='a_fc1_obj') 
            a_fc1_atr = tf.matmul(atr_feat, W_atr_fc1, name='a_fc1_atr')
            a_fc1_q = tf.matmul(q_feat, W_q_fc1, name='a_fc1_q')
            a_explt_fc1 = tf.matmul(concat_explt_feat, W_explt_fc1,
                                    name='a_explt_fc1')        

            a_fc1_region = batchnorm(a_fc1_region, 'reg', train)
            a_fc1_obj = batchnorm(a_fc1_obj, 'obj', train)
            a_fc1_atr = batchnorm(a_fc1_atr, 'atr', train)
            a_fc1_q = batchnorm(a_fc1_q, 'q', train)
            a_explt_fc1 = batchnorm(a_explt_fc1, 'explt', train)
        
            coeff_reg = 0.0
            coeff_obj = 0.0
            coeff_atr = 0.0
            coeff_q = 0.0
            coeff_explt = 0.0

            if mode=='q':
                coeff_q = 1.0

            elif mode=='q_reg':
                coeff_q = 1/2.0
                coeff_reg = 1/2.0

            elif mode=='q_obj_atr':
                coeff_q = 1/4.0
                coeff_obj = 1/4.0
                coeff_atr = 1/4.0
                coeff_explt = 1/4.0

            elif mode=='q_obj_atr_reg':
                coeff_q = 1/5.0
                coeff_obj = 1/5.0
                coeff_atr = 1/5.0
                coeff_reg = 1/5.0
                coeff_explt = 1/5.0

            a_fc1 = coeff_reg * a_fc1_region + \
                    coeff_obj * a_fc1_obj + \
                    coeff_atr * a_fc1_atr + \
                    coeff_q * a_fc1_q + \
                    coeff_explt * a_explt_fc1
            
            h_fc1 = tf.nn.relu(a_fc1, name='h')
            h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob, name='h_drop')

        with tf.name_scope('fc2') as fc2:
            W_feat_fc2 = weight_variable([fc1_dim, 
                                          graph_config['word_vec_dim']],
                                         var_name='W_feat')
            b_feat_fc2 = bias_variable([graph_config['word_vec_dim']],
                                       var_name='b_feat')
            W_ans_fc2 = weight_variable([graph_config['word_vec_dim'], 
                                         graph_config['word_vec_dim']],
                                        var_name='W_ans')      
            b_ans_fc2 = bias_variable([graph_config['word_vec_dim']],
                                      var_name='b_ans')
            comb_feat_embed = tf.add(tf.matmul(h_fc1_drop, W_feat_fc2), 
                                     b_feat_fc2, 
                                     name='comb_feat_embed')
            comb_ans_embed = tf.add(tf.matmul(ans_embed, W_ans_fc2), 
                                    b_ans_fc2, 
                                    name='comb_feat_embed')
            comb_feat_embed = batchnorm(comb_feat_embed, 'feat_embed', train)
            comb_ans_embed = batchnorm(comb_ans_embed, 'ans_embed', train)
        comb_feat_embed = tf.nn.l2_normalize(comb_feat_embed, 1)
        comb_ans_embed = tf.nn.l2_normalize(comb_ans_embed,1)
        ans_scores = tf.matmul(comb_feat_embed, tf.transpose(comb_ans_embed), 
                               name='ans_scores')
        #ans_scores = tf.nn.l2_normalize(ans_scores, 1)*3.0
        return tf.nn.softmax(ans_scores)
        


def aggregate_y_pred(y_pred, region_score, batch_size, num_proposals, 
                     ans_vocab_size):
    y_pred_list = tf.split(0, batch_size, y_pred)
    region_score_list = tf.split(1, batch_size, region_score)
    y_avg_list = []
    for i in xrange(batch_size):
        y_avg_list.append(tf.matmul(region_score_list[i],y_pred_list[i]))
    y_avg = tf.concat(0, y_avg_list)
    return y_avg

        
def evaluation(y, y_pred):
    correct_prediction = tf.equal(tf.argmax(y, 1), tf.argmax(y_pred, 1), 
                                  name='correct_prediction')
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32), 
                              name='accuracy')
    return accuracy


def loss(y, y_pred):
    y_pred_clipped = tf.clip_by_value(y_pred, 1e-10, 1)
    cross_entropy = -tf.reduce_sum(y * tf.log(y_pred_clipped), 
                                   name='cross_entropy')
    batch_size = tf.shape(y)
    return tf.truediv(cross_entropy, tf.cast(batch_size[0],tf.float32))


def margin_loss(y, y_pred, margin):
    correct_score = tf.reduce_sum(tf.mul(y, y_pred), 1, 
                                  keep_dims=True, name='correct_score')
    return tf.reduce_mean(tf.maximum(0.0, y_pred + margin - correct_score))


def regularize_params(param_list):
    regularizer = tf.zeros(shape=[])
    for param in param_list:
        regularizer += tf.nn.l2_loss(param) 
    return regularizer


def batchnorm(input, suffix, is_train, decay=0.95, epsilon=1e-4, name='bn'):
  rank = len(input.get_shape().as_list())
  in_dim = input.get_shape().as_list()[-1]

  if rank == 2:
      axes = [0] 
  elif rank == 4:
      axes = [0, 1, 2]
  else:
      raise ValueError('Input tensor must have rank 2 or 4.')
  
  if suffix:
      suffix = '_' + suffix
  else:
      suffix = ''

  mean, variance = tf.nn.moments(input, axes)
  offset = tf.Variable(initial_value=tf.constant(value=0.0, shape=[in_dim]),
                       name='offset' + suffix)
  scale = tf.Variable(initial_value=tf.constant(value=1.0, shape=[in_dim]),
                      name='scale' + suffix)

  ema = tf.train.ExponentialMovingAverage(decay=decay)
  ema_apply_op = ema.apply([mean, variance])
  ema_mean, ema_var = ema.average(mean), ema.average(variance)

  with tf.control_dependencies([ema_apply_op]):
      bn_train = tf.nn.batch_normalization(input, mean, variance, 
                                           offset, scale, epsilon, name)
  bn_test = tf.nn.batch_normalization(input, ema_mean, ema_var, 
                                      offset, scale, epsilon, name)
  return tf.cond(is_train, lambda : bn_train, lambda : bn_test)


if __name__ == '__main__':
    lg_dir = '/home/tanmay/Code/GenVQA/Exp_Results/lg_files/'

    ans_vocab, _ = ans_io_helper.create_ans_dict()

    train_anno_filename = '/home/tanmay/Code/GenVQA/GenVQA/' + \
                          'shapes_dataset/train_anno.json'

    qa_dict = ans_io_helper.parse_qa_anno(train_anno_filename)
    vocab, inv_vocab = ans_io_helper.get_vocab(qa_dict)

    g = tf.Graph()
    with g.as_default():
        image_regions, questions, keep_prob = \
            placeholder_inputs_ans(len(vocab), len(ans_vocab), mode='no_gt')
        y_pred = obj_comp_graph(image_regions, keep_prob)
        obj_feat = tf.get_collection('obj_feat', scope='obj/conv2')
        y_pred2 = atr_comp_graph(image_regions, keep_prob, obj_feat[0])
        atr_feat = tf.get_collection('atr_feat', scope='atr/conv2')
        y_pred3 = ans_comp_graph(image_regions, questions, keep_prob, \
                                 obj_feat[0], atr_feat[0], 
                                 vocab, inv_vocab, len(ans_vocab))
#        accuracy = evaluation(y, y_pred2)
        sess = tf.Session()
        sess.run(tf.initialize_all_variables())
        # result = sess.run([merged, y_pred], feed_dict={x: np.random.rand(10, 25, 25, 3),
        #                                                y: np.random.rand(10, 4),
        #                                                keep_prob: 1.0})
        
        
