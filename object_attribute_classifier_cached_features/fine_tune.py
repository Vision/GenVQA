import pdb
import os
import ujson

import data.cropped_regions_cached_features as cropped_regions
import tftools.data
from tftools import var_collect, placeholder_management
from object_attribute_classifier_cached_features import inference
from word2vec.word_vector_management import word_vector_manager
import train
import losses
import constants

import tensorflow as tf


def create_initializer(graph, sess, fine_tune_from):
    class initializer():
        def __init__(self):
            with graph.tf_graph.as_default():
                model_restorer = tf.train.Saver(graph.vars_to_save)
                model_restorer.restore(sess, fine_tune_from)    
                var_collect.print_var_list(
                    graph.vars_to_save,
                    'Restored Variables')

                all_vars = tf.all_variables()
                other_vars = [var for var in all_vars
                              if var not in graph.vars_to_save]
                var_collect.print_var_list(
                    other_vars,
                    'optimizer_vars')
                self.init = tf.initialize_variables(other_vars)

        def initialize(self):
            sess.run(self.init)
    
    return initializer()


def fine_tune(
        fine_tune_from_iter,
        batch_generator, 
        sess, 
        initializer,
        vars_to_eval_dict,
        feed_dict_creator,
        logger):

    vars_to_eval_names = []
    vars_to_eval = []
    for var_name, var in vars_to_eval_dict.items():
        vars_to_eval_names += [var_name]
        vars_to_eval += [var]

    with sess.as_default():
        initializer.initialize()

        iter = fine_tune_from_iter+1
        for batch in batch_generator:
            print '---'
            print 'Iter: {}'.format(iter)
            feed_dict = feed_dict_creator(batch)
            eval_vars = sess.run(
                vars_to_eval,
                feed_dict = feed_dict)
            eval_vars_dict = {
                var_name: eval_var for var_name, eval_var in
                zip(vars_to_eval_names, eval_vars)}
            
            logger.log(iter, False, eval_vars_dict)
            iter+=1
        
        logger.log(iter-1, True, eval_vars_dict)

if __name__=='__main__':
    print 'Creating batch generator...'
    batch_generator = train.create_batch_generator()

    print 'Creating computation graph...'
    graph = train.graph_creator(
        constants.region_batch_size,
        constants.tb_log_dir,
        constants.image_size,
        constants.num_object_labels,
        constants.num_attribute_labels,
        constants.region_regularization_coeff,
        resnet_feat_dim=constants.resnet_feat_dim)

    print 'Attaching optimizer...'
    optimizer = train.attach_optimizer(
        graph, constants.region_lr)

    print 'Starting a session...'
    sess = tf.Session(graph=graph.tf_graph)

    print 'Creating initializer...'
    initializer = create_initializer(
        graph, 
        sess,
        constants.region_fine_tune_from)

    print 'Creating feed dict creator...'
    feed_dict_creator = train.create_feed_dict_creator(graph.plh)

    print 'Creating dict of vars to be evaluated...'
    vars_to_eval_dict = {
        'object_prob': graph.obj_atr_inference.object_prob,
        'object_scores': graph.obj_atr_inference.object_scores,
        'attribute_prob': graph.obj_atr_inference.attribute_prob,
        'attribute_scores': graph.obj_atr_inference.attribute_scores,
        'object_accuracy': graph.object_accuracy,
        'attribute_accuracy': graph.attribute_accuracy,
        'total_loss': graph.total_loss,
        'object_loss': graph.object_loss,
        'attribute_loss': graph.attribute_loss,        
        'optimizer_op': optimizer.train_op,
        'merged': graph.merged,
    }

    print 'Creating logger...'
    vars_to_save = graph.vars_to_save
    logger = train.log_mgr(
        graph,
        vars_to_save, 
        sess, 
        constants.region_log_every_n_iter,
        constants.region_output_dir,
        constants.region_model)

    print 'Start training...'
    fine_tune(
        constants.region_fine_tune_from_iter,
        batch_generator, 
        sess, 
        initializer,
        vars_to_eval_dict,
        feed_dict_creator,
        logger)
    
