import pdb

import resnet.inference as resnet_inference
from tftools import var_collect, placeholder_management, layers
import constants
from word2vec.word_vector_management import word_vector_manager
import losses 

import tensorflow as tf


class ObjectAttributeInference():
    def __init__(
            self,
            image_feats, 
            object_label_vectors,
            attribute_label_vectors,
            training):

        self.image_feats = image_feats
        self.object_label_vectors = object_label_vectors
        self.attribute_label_vectors = attribute_label_vectors
        self.training = training
        self.avg_pool_feat = self.image_feats

        self.avg_pool_feat = layers.batch_norm(
            self.avg_pool_feat,
            tf.constant(self.training))

        self.resnet_vars = self.get_resnet_vars()

        self.object_embed = self.add_object_graph(self.avg_pool_feat)
        self.attribute_embed = self.add_attribute_graph(self.avg_pool_feat)
        
        with tf.variable_scope('object_score_graph'):
            self.object_scores = tf.matmul(
                self.object_embed,
                tf.transpose(self.object_label_vectors))
            
            self.object_prob = tf.nn.softmax(
                self.object_scores,
                name = 'object_prob')

        with tf.variable_scope('attribute_score_graph'):
            self.attribute_scores = tf.matmul(
                self.attribute_embed,
                tf.transpose(self.attribute_label_vectors))

            self.attribute_prob = tf.sigmoid(
                self.attribute_scores,
                name = 'attribute_prob')
        
    def get_resnet_vars(self):
        vars_resnet = []
        return vars_resnet

    def add_object_graph(self, input):        
        with tf.variable_scope('object_graph') as object_graph:
            with tf.variable_scope('fc1') as fc1:
                in_dim = input.get_shape().as_list()[-1]
                out_dim = in_dim/2
                fc1_out = layers.full(
                    input,
                    out_dim,
                    'fc',
                    func = None)
                fc1_out = layers.batch_norm(
                    fc1_out,
                    tf.constant(self.training))
                fc1_out = tf.nn.relu(fc1_out) 

            with tf.variable_scope('fc2') as fc2:
                in_dim = fc1_out.get_shape().as_list()[-1]
                out_dim = self.object_label_vectors.get_shape().as_list()[-1]
                fc2_out = layers.full(
                    fc1_out,
                    out_dim,
                    'fc',
                    func = None)
                fc2_out = layers.batch_norm(
                    fc2_out,
                    tf.constant(self.training))
                fc2_out = tf.nn.relu(fc2_out)

        return fc2_out

    def add_attribute_graph(self, input):        
        with tf.variable_scope('attribute_graph') as attribute_graph:
            with tf.variable_scope('fc1') as fc1:
                in_dim = input.get_shape().as_list()[-1]
                out_dim = in_dim/2
                fc1_out = layers.full(
                    input,
                    out_dim,
                    'fc',
                    func = None)
                fc1_out = layers.batch_norm(
                    fc1_out,
                    tf.constant(self.training))
                fc1_out = tf.nn.relu(fc1_out) 

            with tf.variable_scope('fc2') as fc2:
                in_dim = fc1_out.get_shape().as_list()[-1]
                out_dim = self.attribute_label_vectors.get_shape().as_list()[-1]
                fc2_out = layers.full(
                    fc1_out,
                    out_dim,
                    'fc',
                    func = None)
                fc2_out = layers.batch_norm(
                    fc2_out,
                    tf.constant(self.training))
                fc2_out = tf.nn.relu(fc2_out)
        
        return fc2_out
    
    def compute_cosine_similarity(self, feat1, feat2):
        feat1 = tf.nn.l2_normalize(feat1, 1)
        feat2 = tf.nn.l2_normalize(feat2, 1)
        return tf.matmul(feat1, tf.transpose(feat2), name='cosine_similarity')

    def compute_dot_product(self, feat1, feat2):
        return tf.matmul(feat1, tf.transpose(feat2), name='dot_product')

if __name__=='__main__':
    im_h, im_w = constants.image_size

    plh = placeholder_management.PlaceholderManager()
    plh.add_placeholder(
        name = 'image_regions', 
        dtype = tf.float32, 
        shape = [None, im_h, im_w, 3])
    plh.add_placeholder(
        name = 'object_labels',
        dtype = tf.float32,
        shape = [None, constants.num_object_labels])
    plh.add_placeholder(
        name = 'attribute_labels',
        dtype = tf.float32,
        shape = [None, constants.num_attribute_labels])
        
    word_vec_mgr = word_vector_manager()

    training = False
    obj_atr_inference = ObjectAttributeInference(
        plh['image_regions'],
        word_vec_mgr.object_label_vectors,
        word_vec_mgr.attribute_label_vectors,
        training)
    
    object_loss = losses.object_loss(
        obj_atr_inference.object_scores, 
        plh['object_labels'])
    
    attribute_loss = losses.attribute_loss(
        obj_atr_inference.attribute_scores, 
        plh['attribute_labels'])

    vars_to_regularize = tf.get_collection('to_regularize')
    var_collect.print_var_list(vars_to_regularize, 'to_regularize')

    
    
    
