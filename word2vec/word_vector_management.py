import numpy as np
import pdb
import json

from tftools import var_collect, placeholder_management, layers
import constants

import tensorflow as tf


class word_vector_manager():
    def __init__(self):
        self.vocab_word_vectors = np.load(
            constants.pretrained_vocab_word_vectors_npy)
        self.vocab_word_vectors = self.vocab_word_vectors.astype(np.float32)
        self.vocab_size = self.vocab_word_vectors.shape[0]
        
        self.read_object_labels()
        self.read_attribute_labels()
        self.read_vocab()

        with tf.variable_scope('word_vectors') as word_vectors:
            self.init_word_vector_tensor()
            # self.normalized_word_vectors = tf.nn.l2_normalize(
            #     self.word_vectors, 1)

            with tf.variable_scope('object_label_word_vectors'):
                self.object_label_word_vectors()
            
            with tf.variable_scope('attribute_label_word_vectors'):
                self.attribute_label_word_vectors()


    def init_word_vector_tensor(self):
#        with tf.device('/cpu:0'):
        self.word_vectors = tf.get_variable(
            name = 'word_vector',
            shape = [self.vocab_size, constants.word_vector_size],
            initializer = tf.constant_initializer(self.vocab_word_vectors))

        tf.add_to_collection('to_regularize', self.word_vectors)

    def read_object_labels(self):
        with open(constants.object_labels_json, 'r') as file:
            self.object_labels = json.load(file)

    def read_attribute_labels(self):
        with open(constants.attribute_labels_json, 'r') as file:
            self.attribute_labels = json.load(file)

    def read_vocab(self):
        with open(constants.vocab_json, 'r') as file:
            self.vocab = json.load(file)

    def create_phrase_word_vectors(self, phrase, scope_name):
        with tf.variable_scope(scope_name) as phrase_graph:
            words = phrase.split(" ")
            ids = []
            for word in words:
                if word in self.vocab:
                    ids += [self.vocab[word]]
                else:
                    ids += [self.vocab[constants.unknown_token]]

            phrase_word_vector = tf.nn.embedding_lookup(
                self.word_vectors,
                tf.constant(ids, dtype=tf.int64),
                name = 'embedding_lookup')

            phrase_word_vector = tf.reduce_mean(
                phrase_word_vector, 
                0,
                True,
                'reduce_mean')

        return phrase_word_vector
                
    def object_label_word_vectors(self):
        inv_object_labels = {v: k for k, v in self.object_labels.items()}
        num_object_labels = len(inv_object_labels)

        object_label_vector_list = [None]*num_object_labels
        for i in xrange(num_object_labels):
            object_label_vector_list[i] = self.create_phrase_word_vectors(
                inv_object_labels[i],
                'object_label_' + str(i))

        self.object_label_vectors = tf.concat(
            0, object_label_vector_list)

    def attribute_label_word_vectors(self):
        inv_attribute_labels = {v: k for k, v in self.attribute_labels.items()}
        num_attribute_labels = len(inv_attribute_labels)

        attribute_label_vector_list = [None]*num_attribute_labels
        for i in xrange(num_attribute_labels):
            attribute_label_vector_list[i] = self.create_phrase_word_vectors(
                inv_attribute_labels[i],
                'attribute_label_' + str(i))
        
        self.attribute_label_vectors = tf.concat(
            0, attribute_label_vector_list)
        
if __name__=='__main__':
    word_vector_mgr = word_vector_manager()

    
        
