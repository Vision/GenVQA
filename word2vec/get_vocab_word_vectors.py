from gensim.models import word2vec
import numpy as np
import ujson
import pdb
import constants

def get_vocab_word_vectors(
        model,
        vocab):
    
    vocab_size = len(vocab)

    assert_str = 'predefined word vector size does not match model'
    assert (model['test'].shape[0]==constants.word_vector_size), assert_str

    vocab_word_vectors = 2*np.random.rand(
        vocab_size, 
        constants.word_vector_size)
    vocab_word_vectors -= 1.0

    found_word_vec = 0
    for word, index  in vocab.items():
        if word in model:
            found_word_vec += 1
            vocab_word_vectors[index,:] = model[word]
            
    np.save(
        constants.pretrained_vocab_word_vectors_npy, 
        vocab_word_vectors)

    print 'Found word vectors for {} out of {} words'.format(
        found_word_vec,
        vocab_size)


if __name__=='__main__':
    model = word2vec.Word2Vec.load_word2vec_format(
        constants.word2vec_binary, 
        binary=True)

    with open(constants.vocab_json, 'r') as file:
        vocab = ujson.load(file)

    get_vocab_word_vectors(model, vocab)

    

