import os
import pdb
import ujson
import matplotlib.pyplot as plt
import numpy as np


if __name__=='__main__':
    num_pos_examples = []
    for i in xrange(10):
        dirname = '/home/tanmay/Code/GenVQA/Exp_Results/VisualGenome/' + \
                  'object_attribute_with_compensation_with_word_vector_prediction_high_lr/' + \
                  'object_attribute_classifiers/attribute_scores/'

        labels_filename = os.path.join(dirname, 'labels_' + str(i) + '.json')
        scores_filename = os.path.join(dirname, 'scores_' + str(i) + '.json')

        with open(labels_filename, 'r') as file:
            labels = ujson.load(file)

        with open(scores_filename, 'r') as file:
            scores = ujson.load(file)

        labels_list = []
        for batch_labels in labels:
            labels_list += batch_labels
            
        scores_list = []
        for batch_scores in scores:
            scores_list += batch_scores

        def getKey(item):
            return item[0]

        sorted_list = sorted(
            zip(scores_list, labels_list),
            key = getKey,
            reverse=True)

        sorted_scores, sorted_labels = zip(*sorted_list)
        tp = np.float32(np.cumsum(sorted_labels))
        pred_p = np.arange(len(sorted_labels)) +  1.0
        
        precision = tp/pred_p
        recall = tp/tp[-1]
        
        num_pos_examples.append(tp[-1])        

        plt.plot(recall, precision, '-')
        plt.axis([0, 1.0, 0, 1.0])

        figname = os.path.join(
            dirname, 
            'PR_' + str(i) + '.pdf')

        plt.savefig(figname)

        plt.clf()
        
    plt.plot(xrange(10), num_pos_examples, '-')
    
    figname = os.path.join(
        dirname,
        'num_pos_examples.pdf')

    plt.savefig(figname)

    plt.close()
