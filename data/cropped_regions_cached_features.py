import numpy as np
#import json
import ujson
import os
import pdb
import time
import threading

import tftools.data 
import image_io
import constants

import tensorflow as tf

_unknown_token = constants.unknown_token

def unwrap_self_get_single(arg, **kwarg):
    return data.get_single(*arg, **kwarg)

class data():
    def __init__(self,
                 feat_dir,
                 image_dir,
                 object_labels_json,
                 attribute_labels_json,
                 regions_json,
                 region_ids_json,
                 image_size,
                 channels=3,
                 resnet_feat_dim=2048,
                 mean_image_filename=None):
        self.feat_dir = feat_dir
        self.image_dir = image_dir
        self.h = image_size[0]
        self.w = image_size[1]
        self.c = channels
        self.resnet_feat_dim = resnet_feat_dim
        self.mean_image = self.get_mean_image(mean_image_filename)
        self.object_labels_dict = self.read_json_file(object_labels_json)
        self.attribute_labels_dict = self.read_json_file(attribute_labels_json)
        self.inv_object_labels_dict = self.invert_label_dict(
            self.object_labels_dict)
        self.inv_attribute_labels_dict = self.invert_label_dict(
            self.attribute_labels_dict)
        self.num_object_labels = len(self.object_labels_dict)
        self.num_attribute_labels = len(self.attribute_labels_dict)
        self.regions = self.read_json_file(regions_json)
        self.region_ids = self.read_json_file(region_ids_json)
        self.num_regions = len(self.regions)
        self.create_sample_to_region_dict()
        self.num_threads = 0

    def create_sample_to_region_dict(self):
        self.sample_to_region_dict = \
            {k: v for k, v in zip(xrange(len(self.region_ids)),
                                  self.region_ids)}

        # self.sample_to_region_dict = \
        #     {k: v for k, v in zip(xrange(self.num_regions),
        #                           self.regions.keys())}

    def invert_label_dict(self, label_dict):
        return {v: k for k, v in label_dict.items()}

    def read_json_file(self, filename):
        print 'Reading {} ...'.format(filename)
        with open(filename, 'r') as file:
            return ujson.load(file)

    def get_single(self, sample, batch_list, worker_id):
        try:
            batch = dict()
            batch['region_id'] = self.sample_to_region_dict[sample]
            batch['region_feats'], read_success = self.get_region_feats(sample)
            if read_success:
                batch['object_label'], batch['object_label_words'] = \
                    self.get_object_label(sample)
                batch['attribute_label'], batch['attribute_label_words'] = \
                    self.get_attribute_label(sample)
            else:
                batch['region_feats'] = np.zeros(
                    [1, self.resnet_feat_dim], np.float32)
                batch['object_label'] = np.zeros(
                    [len(self.object_labels_dict)], np.float32)
                batch['attribute_label'] = np.zeros(
                    [len(self.attribute_labels_dict)], np.float32)
                batch['object_label_words'] = []
                batch['attribute_label_words'] = []

            batch_list[worker_id] = batch

        except Exception, e:
            print 'Error in thread {}: {}'.format(
                threading.current_thread().name, str(e))


    def get_parallel(self, samples):
        batch_list = [None]*len(samples)
        worker_ids = range(len(samples))
        workers = []
        for count, sample in enumerate(samples):
            self.get_single(sample, batch_list, worker_ids[count])
        #     worker = threading.Thread(
        #         target = self.get_single, 
        #         args = (sample, batch_list, worker_ids[count]))
        #     worker.setDaemon(True)
        #     worker.start()
        #     workers.append(worker)
        
        # for worker in workers:
        #     worker.join()

        batch_size = len(samples)
        batch = dict()
        batch['region_ids'] = dict()
        batch['region_feats'] = np.zeros(
            [batch_size, self.resnet_feat_dim], np.float32)
        batch['object_labels']= np.zeros(
            [batch_size, len(self.object_labels_dict)], np.float32)
        batch['attribute_labels'] = np.zeros(
            [batch_size, len(self.attribute_labels_dict)], np.float32)

        for index, single_batch in enumerate(batch_list):
            batch['region_ids'][index] = single_batch['region_id']
            batch['region_feats'][index, :] = single_batch['region_feats']
            batch['object_labels'][index, :] = single_batch['object_label']
            batch['attribute_labels'][index,:] = single_batch['attribute_label']

        return batch

    def get_region_feats(self, sample):
        region_id = self.sample_to_region_dict[sample]
        region = self.regions[region_id]
        image_subdir = os.path.join(self.feat_dir,
                                    str(region['image_id']))
        filename = os.path.join(image_subdir,
                                str(region_id) + '.npy')
        read_success = True
        try:
            region_feats = np.load(filename)
        except:
            print 'Could not read image: Setting the image pixels to 0s'
            read_success = False
            region_feats = np.zeros([1, self.resnet_feat_dim], dtype=np.float32)

        return region_feats, read_success

    def get_region_image(self, sample):
        region_id = self.sample_to_region_dict[sample]
        region = self.regions[region_id]
        image_subdir = os.path.join(self.image_dir,
                                    str(region['image_id']))
        filename = os.path.join(image_subdir,
                                str(region_id) + '.jpg')
        read_success = True
        try:
            region_image = image_io.imread(filename)
            region_image = region_image.astype(np.float32)
        except:
            print 'Could not read image: Setting the image pixels to 0s'
            read_success = False
            region_image = np.zeros([self.h, self.w, 3], dtype=np.float32)

        return region_image, read_success

    def single_to_three_channel(self, image):
        if len(image.shape)==3:
            return image
        elif len(image.shape)==2:
            im_h, im_w =image.shape
            image_tmp = np.zeros([im_h, im_w, 3], dtype=image.dtype)
            for c in xrange(3):
                image_tmp[:,:,c] = image
            return image_tmp

    def get_clipped_region_coords(self, region, image_size):
        im_h, im_w = image_size
        x = min(im_w - 1, max(0, region["x"]))
        y = min(im_h - 1, max(0, region["y"]))
        h = min(im_h - y, max(region["h"], 1))
        w = min(im_w - x, max(region["w"], 1))
        return x, y, w, h

    def get_mean_image(self, mean_image_filename):
        if mean_image_filename:
            return image_io.imread(mean_image_filename).astype(
                np.float32)
        else:
            return np.zeros([self.h, self.w, self.c], np.float32)

    def get_object_label(self, sample):
        # Returns a multihot vector encoding of object labels
        # If an object label is not found in the labels list, 
        # _unknown_token is produced in that case. 
        region_id = self.sample_to_region_dict[sample]
        region = self.regions[region_id]
        object_labels = region['object_names']
        object_label_encoding = np.zeros([1, self.num_object_labels], 
                                         dtype = np.float32)
        if object_labels:
            for object in object_labels:
                if object not in self.object_labels_dict:
                    label_id = self.object_labels_dict[_unknown_token]
                else:
                    label_id = self.object_labels_dict[object]
                object_label_encoding[0,label_id] = 1.0
        else:
            label_id = self.object_labels_dict[_unknown_token]
            object_label_encoding[0,label_id] = 1.0

        return object_label_encoding/np.sum(object_label_encoding), object_labels
            
    def get_attribute_label(self, sample):
        # Attribute is turned on if it is present 
        region_id = self.sample_to_region_dict[sample]
        region = self.regions[region_id]
        attribute_labels = region['attributes']
        attribute_label_encoding = np.zeros([1, self.num_attribute_labels], 
                                            dtype = np.float32)
        for attribute in attribute_labels:
            if attribute in self.attribute_labels_dict:
                label_id = self.attribute_labels_dict[attribute]
                attribute_label_encoding[0,label_id] = 1.0

        return attribute_label_encoding, attribute_labels

if __name__=='__main__':
    data_mgr = data(constants.genome_resnet_feat_dir,
                    constants.image_dir,
                    constants.object_labels_json,
                    constants.attribute_labels_json,
                    constants.regions_json,
                    constants.image_size,
                    channels=3,
                    mean_image_filename=None)
    print 'Number of object labels: {}'.format(data_mgr.num_object_labels)
    print 'Number of attribute labels: {}'.format(data_mgr.num_attribute_labels)
    print 'Number of regions: {}'.format(data_mgr.num_regions)

    #Test sample
    samples = [1, 2]
    sample = samples[0]
    region_id = data_mgr.sample_to_region_dict[sample]
    region = data_mgr.regions[region_id]
    attribute_encoding,_ = data_mgr.get_attribute_label(sample)
    object_encoding,_ = data_mgr.get_object_label(sample)
    region_image = data_mgr.get_region_image(sample)

    attributes = []
    for i in xrange(attribute_encoding.shape[1]):
        if attribute_encoding[0,i] > 0 :
            attributes.append(data_mgr.inv_attribute_labels_dict[i])

    objects = []
    for i in xrange(object_encoding.shape[1]):
        if object_encoding[0,i] > 0 :
            objects.append(data_mgr.inv_object_labels_dict[i])
    
    print "Region: {}".format(region)
    print "Attributes: {}".format(", ".join(attributes))
    print "Objects: {}".format(", ".join(objects))

    batch_size = 200
    num_samples = 200
    num_epochs = 1
    offset = 0
    queue_size = 100

    index_generator = tftools.data.random(
        batch_size, 
        num_samples, 
        num_epochs, 
        offset)
    
    batch_generator = tftools.data.async_batch_generator(
        data_mgr, 
        index_generator, 
        queue_size)

    count = 0 
    for batch in batch_generator:
        print 'Batch Number: {}'.format(count)
        count += 1
        pdb.set_trace()

    
