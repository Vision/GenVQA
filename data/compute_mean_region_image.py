import numpy as np
from data.regions import data
import constants
import image_io
import pdb


_image_size = constants.image_size
_num_train_regions = constants.num_train_regions
_mean_image_filename = constants.mean_image_filename


if __name__=='__main__':
    data_mgr = data(constants.image_dir,
                    constants.object_labels_json,
                    constants.attribute_labels_json,
                    constants.regions_json,
                    constants.image_size,
                    channels=3,
                    mean_image_filename=None)

    h, w = _image_size
    mean_image = np.zeros([h, w, 3], dtype=np.float32)
    num_images_for_mean = min(10000, _num_train_regions)
    for i in xrange(num_images_for_mean):
        mean_image += data_mgr.get_region_image(i)
    mean_image = mean_image/num_images_for_mean
    image_io.imwrite(np.uint8(mean_image*255),
                     _mean_image_filename)

    
