import numpy as np
import json
import os
import pdb
import time

from multiprocessing import Pool

import tftools.data 
import image_io
import constants

import tensorflow as tf

_unknown_token = constants.unknown_token

class data():
    def __init__(self,
                 image_dir,
                 object_labels_json,
                 attribute_labels_json,
                 regions_json,
                 image_size,
                 channels=3,
                 mean_image_filename=None):
        self.image_dir = image_dir
        self.h = image_size[0]
        self.w = image_size[1]
        self.c = channels
        self.mean_image = self.get_mean_image(mean_image_filename)
        self.object_labels_dict = self.read_json_file(object_labels_json)
        self.attribute_labels_dict = self.read_json_file(attribute_labels_json)
        self.inv_object_labels_dict = self.invert_label_dict(
            self.object_labels_dict)
        self.inv_attribute_labels_dict = self.invert_label_dict(
            self.attribute_labels_dict)
        self.num_object_labels = len(self.object_labels_dict)
        self.num_attribute_labels = len(self.attribute_labels_dict)
        self.regions = self.read_json_file(regions_json)
        self.num_regions = len(self.regions)
        self.create_sample_to_region_dict()

    def create_sample_to_region_dict(self):
        self.sample_to_region_dict = \
            {k: v for k, v in zip(xrange(self.num_regions),
                                  self.regions.keys())}

    def invert_label_dict(self, label_dict):
        return {v: k for k, v in label_dict.items()}

    def read_json_file(self, filename):
        print 'Reading {} ...'.format(filename)
        with open(filename, 'r') as file:
            return json.load(file)

    def get(self, samples):
        batch_size = len(samples)
        batch = dict()
        batch['region_ids'] = dict()
        batch['images'] = np.zeros(
            [batch_size, self.h, self.w, self.c], np.float32)
        batch['object_labels'] = np.zeros(
            [batch_size, len(self.object_labels_dict)], np.float32)
        batch['attribute_labels'] = np.zeros(
            [batch_size, len(self.attribute_labels_dict)], np.float32)

        for index, sample in enumerate(samples):
            batch['region_ids'][index] = self.sample_to_region_dict[sample]
            batch['images'][index, :, :, :] = self.get_region_image(sample)
            batch['object_labels'][index, :] = self.get_object_label(sample)
            batch['attribute_labels'][index,:] = \
                self.get_attribute_label(sample)

        return batch
    
    def get_single(self, sample):
        print sample
        batch = dict()
        batch['region_ids'] = dict()
        batch['images'] = np.zeros(
            [self.h, self.w, self.c], np.float32)
        batch['object_labels'] = np.zeros(
            [len(self.object_labels_dict)], np.float32)
        batch['attribute_labels'] = np.zeros(
            [len(self.attribute_labels_dict)], np.float32)

        batch['region_ids'] = self.sample_to_region_dict[sample]
        batch['images'] = self.get_region_image(sample)
        batch['object_labels'] = self.get_object_label(sample)
        batch['attribute_labels'] = self.get_attribute_label(sample)

        return batch

    def get_parallel(self, samples):
        batch_list = self.pool.map(self.get_single, samples)
        batch_size = len(samples)
        batch = dict()
        batch['region_ids'] = dict()
        batch['images'] = np.zeros(
            [batch_size, self.h, self.w, self.c], np.float32)
        batch['object_labels'] = np.zeros(
            [batch_size, len(self.object_labels_dict)], np.float32)
        batch['attribute_labels'] = np.zeros(
            [batch_size, len(self.attribute_labels_dict)], np.float32)

        for index, single_batch in enumerate(batch_list):
            batch['region_ids'][index] = single_batch['region_ids']
            batch['images'][index, :, :, :] = single_batch['images']
            batch['object_labels'][index, :] = single_batch['object_labels']
            batch['attribute_labels'][index,:] = single_batch['attribute_labels']


        return batch
        
    def get_region_image(self, sample):
        region_id = self.sample_to_region_dict[sample]
        region = self.regions[region_id]
        filename = os.path.join(self.image_dir,
                                str(region['image_id']) + '.jpg')
        image = image_io.imread(filename)
        image = self.single_to_three_channel(image)
        x, y, w, h = self.get_clipped_region_coords(region, image.shape[0:2])
        region_image = image[y:y + h, x:x + w, :]

        region_image = image_io.imresize(
            region_image,
            output_size=(self.h, self.w)).astype(np.float32)

        return region_image / 255 - self.mean_image

    def single_to_three_channel(self, image):
        if len(image.shape)==3:
            return image
        elif len(image.shape)==2:
            im_h, im_w =image.shape
            image_tmp = np.zeros([im_h, im_w, 3], dtype=image.dtype)
            for c in xrange(3):
                image_tmp[:,:,c] = image
            return image_tmp

    def get_clipped_region_coords(self, region, image_size):
        im_h, im_w = image_size
        x = min(im_w - 1, max(0, region["x"]))
        y = min(im_h - 1, max(0, region["y"]))
        h = min(im_h - y, max(region["h"], 1))
        w = min(im_w - x, max(region["w"], 1))
        return x, y, w, h

    def get_mean_image(self, mean_image_filename):
        if mean_image_filename:
            return image_io.imread(mean_image_filename).astype(
                np.float32) / 255
        else:
            return np.zeros([self.h, self.w, self.c], np.float32)

    def get_object_label(self, sample):
        # Returns a multihot vector encoding of object labels
        # If an object label is not found in the labels list, 
        # _unknown_token is produced in that case. 
        region_id = self.sample_to_region_dict[sample]
        region = self.regions[region_id]
        object_labels = region['object_names']
        object_label_encoding = np.zeros([1, self.num_object_labels], 
                                         dtype = np.float32)
        if object_labels:
            for object in object_labels:
                if object not in self.object_labels_dict:
                    label_id = self.object_labels_dict[_unknown_token]
                else:
                    label_id = self.object_labels_dict[object]
                object_label_encoding[0,label_id] = 1.0
        else:
            label_id = self.object_labels_dict[_unknown_token]
            object_label_encoding[0,label_id] = 1.0

        return object_label_encoding/np.sum(object_label_encoding)
            
    def get_attribute_label(self, sample):
        # Attribute is turned on if it is present 
        region_id = self.sample_to_region_dict[sample]
        region = self.regions[region_id]
        attribute_labels = region['attributes']
        attribute_label_encoding = np.zeros([1, self.num_attribute_labels], 
                                            dtype = np.float32)
        for attribute in attribute_labels:
            if attribute in self.attribute_labels_dict:
                label_id = self.attribute_labels_dict[attribute]
                attribute_label_encoding[0,label_id] = 1.0

        return attribute_label_encoding

if __name__=='__main__':
    data_mgr = data(constants.image_dir,
                    constants.object_labels_json,
                    constants.attribute_labels_json,
                    constants.regions_json,
                    constants.image_size,
                    channels=3,
                    mean_image_filename=None)
    print 'Number of object labels: {}'.format(data_mgr.num_object_labels)
    print 'Number of attribute labels: {}'.format(data_mgr.num_attribute_labels)
    print 'Number of regions: {}'.format(data_mgr.num_regions)

    #Test sample
    samples = [1, 2]
    sample = samples[0]
    region_id = data_mgr.sample_to_region_dict[sample]
    region = data_mgr.regions[region_id]
    attribute_encoding = data_mgr.get_attribute_label(sample)
    object_encoding = data_mgr.get_object_label(sample)
    region_image = data_mgr.get_region_image(sample)

    attributes = []
    for i in xrange(attribute_encoding.shape[1]):
        if attribute_encoding[0,i] > 0 :
            attributes.append(data_mgr.inv_attribute_labels_dict[i])

    objects = []
    for i in xrange(object_encoding.shape[1]):
        if object_encoding[0,i] > 0 :
            objects.append(data_mgr.inv_object_labels_dict[i])
    
    print "Region: {}".format(region)
    print "Attributes: {}".format(", ".join(attributes))
    print "Objects: {}".format(", ".join(objects))
    
#    image_io.imshow(region_image)

    batch_size = 200
    num_samples = 1000
    num_epochs = 1
    offset = 0

    index_generator = tftools.data.random(
        batch_size, 
        num_samples, 
        num_epochs, 
        offset)
    
    # start = time.time()
    # count = 0
    # for samples in index_generator:
    #     batch = data_mgr.get(samples)
    #     print 'Batch Count: {}'.format(count)
    #     count += 1
    # stop = time.time()
    # print 'Time per batch: {}'.format((stop-start)/5.0)

    batch_generator = tftools.data.async_batch_generator(
        data_mgr, 
        index_generator, 
        1000)

    count = 0 
    start = time.time()
    for batch in batch_generator:
        print 'Batch Number: {}'.format(count)
#        print batch['region_ids']
        count += 1
    stop = time.time()
    print 'Time per batch: {}'.format((stop-start)/50.0)
    print "Count: {}".format(count)

    pool.close()
