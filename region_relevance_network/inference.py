import tensorflow as tf


class RegionRelevanceInference():
    def __init__(
            self,
            batch_size,
            object_feat,
            attribute_feat,
            nouns,
            adjectives):
        
        self.batch_size = batch_size
        self.object_feat = object_feat
        self.attribute_feat = attribute_feat
        self.nouns = nouns
        self.adjectives = adjectives
        
        with tf.variable_scope('region_relevance_graph'):
            # Compute dot product between region and qa nouns
            ordered_keys = ['positive_nouns']
            for i in xrange(len(nouns)-1):
                ordered_keys.append('negative_nouns_' + str(i))

            self.noun_object_scores_list = [[] for i in xrange(self.batch_size)]
            self.noun_object_scores = [None]*self.batch_size
            for j in xrange(self.batch_size):
                for key in ordered_keys:
                    self.noun_object_scores_list[j] += [tf.reduce_max(
                        tf.matmul(
                            self.nouns[key][j],
                            tf.transpose(self.object_feat[j])),
                        0,
                        keep_dims=True)]
                
                self.noun_object_scores[j] = tf.concat(
                    0,
                    self.noun_object_scores_list[j],
                )

            # Compute dot product between region and qa adjectives
            ordered_keys = ['positive_adjectives']
            for i in xrange(len(nouns)-1):
                ordered_keys.append('negative_adjectives_' + str(i))

            self.adjective_attribute_scores_list = [[] for i in xrange(self.batch_size)]
            self.adjective_attribute_scores = [None]*self.batch_size
            for j in xrange(self.batch_size):
                for key in ordered_keys:
                    self.adjective_attribute_scores_list[j] += [tf.reduce_max(
                        tf.matmul(
                            self.adjectives[key][j],
                            tf.transpose(self.attribute_feat[j])),
                        0,
                        keep_dims=True)]

                self.adjective_attribute_scores[j] = tf.concat(
                    0,
                    self.adjective_attribute_scores_list[j],
            )

            self.answer_region_scores = [None]*self.batch_size
            self.answer_region_prob = [None]*self.batch_size
            for j in xrange(self.batch_size):
                self.answer_region_scores[j] = \
                    self.noun_object_scores[j] + \
                    self.adjective_attribute_scores[j]
            
                self.answer_region_prob[j] = tf.nn.softmax(
                    self.answer_region_scores[j],
                    'region_relevance_prob')

            
            
        
                                
