from vqa_eval import analyzer
import constants

if __name__=='__main__':
    analyzer.analyze(
        constants.raw_vqa_val_anno_json,
        constants.raw_vqa_val_ques_json,
        constants.answer_eval_results_json,
        constants.vqa_results_dir)

